import { DateEntity } from './Entity/date.entity';
import { FloorController } from './Controlers/floor.controller';
import { DeskController } from './Controlers/desk.controller';
import { FloorService } from './Services/floor.service';
import { DeskService } from './Services/desk.service';
import { CovidService } from './Services/covid.service';
import { TransformService } from './Services/transform.service';
import { HttpModule, Module } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { PassportModule } from "@nestjs/passport";
import { TypeOrmModule } from "@nestjs/typeorm";
import { jwtConstants } from "./Auth/secret";
import { JwtStrategy } from "./Auth/strategy";
import { CovidController } from "./Controlers/covid-controller";
import { ProjectsController } from "./Controlers/projects.controller";
import { UsersController } from "./Controlers/users.controller";
import { CovidEntity } from "./Entity/covid.entity";
import { CurrentWeekEntity } from "./Entity/currentweek.entity";
import { DeskEntity } from "./Entity/desks.entity";
import { FloorEntity } from "./Entity/floor.entity";
import { NextWeekEntity } from "./Entity/nextweek.entity";
import { ProjectsEntity } from "./Entity/projects.entity";
import { TokenEntity } from "./Entity/token.entity";
import { UsersEntity } from "./Entity/users.entity";
import { ProjectService } from "./Services/projects.service";
import { UsersService } from "./Services/users.service";
import { NextWeekController } from './Controlers/next-week.controller';
import { CurrentWeekController } from './Controlers/current-week.controller';
import { CurrentWeekService } from './Services/current-week.service';
import { NextWeekService } from './Services/next-week.service';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mariadb',
      host: 'localhost',
      port: 3306,
      username: 'root',
      password: '1234',
      database: 'finalprojectdb',
      entities: [ NextWeekEntity, CurrentWeekEntity, DeskEntity, ProjectsEntity, TokenEntity,
        UsersEntity,  CovidEntity, FloorEntity, NextWeekEntity, DateEntity],
      synchronize: true,
    }),
    TypeOrmModule.forFeature([ NextWeekEntity, CurrentWeekEntity, DeskEntity, ProjectsEntity,
      TokenEntity, UsersEntity, CovidEntity, FloorEntity, NextWeekEntity, DateEntity]),
    PassportModule,
    HttpModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: '7d' },
    }),
  ],

  controllers: [NextWeekController, CurrentWeekController, ProjectsController, UsersController, CovidController, DeskController, FloorController],
  providers: [NextWeekService, CurrentWeekService, ProjectService, TransformService, UsersService, JwtStrategy, CovidService, DeskService, FloorService],
})
export class AppModule { }
