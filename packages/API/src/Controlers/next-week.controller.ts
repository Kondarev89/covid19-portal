import { Get, Post, Body, ValidationPipe, Put, Request, UseGuards, Param, Controller } from "@nestjs/common";
import { BlacklistGuard } from "src/Auth/blacklist.guard";
import { RolesGuard } from "src/Auth/roles.guard";
import { UserId } from "src/Auth/user-id.decorator";
import { NextWeekDTO } from "src/Dtos/NextWeek.dto";
import { UserRole } from "src/Enums/users-role";
import { NextWeekService } from "src/Services/next-week.service";

@Controller('nextWeek')
export class NextWeekController {
    constructor(
        private readonly service: NextWeekService,
       ) {}

       @Post()
       //@UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
       public async createCurrentWeek(@Body(new ValidationPipe({ whitelist: true })) project: NextWeekDTO, userid: number): Promise<NextWeekDTO> {
         return await this.service.createNextWeek(project,userid);
       }

    
}