import { Body, Controller, Get, Request, Param, Post, Put, UseGuards, ValidationPipe } from "@nestjs/common";
import { BlacklistGuard } from "src/Auth/blacklist.guard";
import { RolesGuard } from "src/Auth/roles.guard";
import { UserId } from "src/Auth/user-id.decorator";
import { CurrentWeekDTO } from "src/Dtos/currentWeek.dto";
import { UsersDTO } from "src/Dtos/users.dto";
import { UserRole } from "src/Enums/users-role";
import { CurrentWeekService } from "src/Services/current-week.service";

@Controller('currentWeek')
export class CurrentWeekController {

    constructor(
        private readonly service: CurrentWeekService,
       ) {}
       @Post()
       //@UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
       public async createCurrentWeek(@Body(new ValidationPipe({ whitelist: true })) project: CurrentWeekDTO, userid: number): Promise<CurrentWeekDTO> {
         return await this.service.createCurrentWeek(project,userid);
       }
}