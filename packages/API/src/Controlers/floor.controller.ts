import { FloorService } from './../Services/floor.service';
import { Controller, Get, Param, UseGuards, Body, Delete, Post, Put, ValidationPipe } from '@nestjs/common';
import { FloorDTO } from 'src/Dtos/floor.dto';
import { BlacklistGuard } from 'src/Auth/blacklist.guard';
import { RolesGuard } from '../Auth/roles.guard';
import { UserRole } from 'src/Enums/users-role';

@Controller('floors')
export class FloorController {
    constructor(private readonly service: FloorService) { }


    @Get()
    public async allProjects(): Promise<FloorDTO[]> {
        const projects = await this.service.findAll();
        return projects;
    }


    @Post()
    // @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async createFloor(@Body(new ValidationPipe({ transform: true, whitelist: true })) floor: FloorDTO): Promise<FloorDTO> {
        return await this.service.createOne(floor);
    }

    @Put(':id')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async updateDesk(
        @Param('id') floorId: number,
        @Body(new ValidationPipe({ whitelist: true })) floor: FloorDTO): Promise<FloorDTO> {
        return await this.service.updateOne(floorId, floor);
    }

    @Delete(':id')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async deleteFloor(
        @Param('id') deskId: number): Promise<{ msg: string }> {
        return await this.service.deleteOne(deskId);
    }
}
