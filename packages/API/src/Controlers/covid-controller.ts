import { CovidService } from './../Services/covid.service';
import { HttpService, Controller, Get, Body, NotFoundException, Param, Post, Put, ValidationPipe, UseGuards } from "@nestjs/common";
import { CovidDTO } from 'src/Dtos/covid.dto';
import { BlacklistGuard } from '../Auth/blacklist.guard';
import { RolesGuard } from '../Auth/roles.guard';
import { UserRole } from '../Enums/users-role';

@Controller('covid')
export class CovidController {
    constructor(private readonly service: CovidService, private readonly http: HttpService) { }

    @Get()
    // @UseGuards(AuthGuard('jwt'))
    public async allCountries(): Promise<CovidDTO[]> {
        const countries = await this.service.getAll();
        return countries;
    }

    @Get(':id')
    // @UseGuards(AuthGuard('jwt'))
    public async getById(@Param('id') id: number): Promise<CovidDTO> {
        const country = await this.service.getById(id);
        if (country === undefined) {
            throw new NotFoundException(`No covid country with id ${id}`);
        }
        return country;
    }

    @Post()
    // @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async createCovid(@Body(new ValidationPipe({ whitelist: true }))
    covid: CovidDTO): Promise<CovidDTO> {
        const response: CovidDTO = await this.http.get('https://coronavirus-19-api.herokuapp.com/countries').toPromise()
            .then(res => res.data);
        return await this.service.createCovid(response, covid);
    }

    @Put(':id')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async updateCovid(
        @Param('id') countryId: number,
        @Body(new ValidationPipe({ whitelist: true })) country: CovidDTO): Promise<CovidDTO> {
        return await this.service.updateCovid(countryId, country);
    }

    // @Delete(':id')
    // public async deleteCovid(
    //     @Param('id') countryId: number): Promise<{ msg: string }> {
    //     return await this.service(countryId);
    // }
}
