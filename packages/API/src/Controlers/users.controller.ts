import { UsersService } from './../Services/users.service';
import { Controller, Post, Body, BadRequestException, ValidationPipe, Delete, UseInterceptors, UploadedFile, Param, Get, Query, UseGuards, NotFoundException } from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import { extname } from 'path';
import { GetToken } from '../Auth/get-token.decorator';
import { UsersRegisterDTO } from '../Dtos/users-register.dto';
import { UsersLoginDTO } from '../Dtos/users-login.dto';
import { UsersDTO } from '../Dtos/users.dto';
import { BlacklistGuard } from '../Auth/blacklist.guard';
import { RolesGuard } from '../Auth/roles.guard';
import { UserRole } from '../Enums/users-role';

@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService) { }


  @Post('login')
  async login(@Body(new ValidationPipe({ transform: true, whitelist: true })) user: UsersLoginDTO): Promise<{ token: string }> {
    const token = await this.userService.login(user);
    if (!token) {
      throw new BadRequestException(`Invalid email and/or password!`);
    }
    return { token };
  }

  @Post()
  async register(@Body(new ValidationPipe({ whitelist: true })) usersInput: UsersRegisterDTO): Promise<UsersDTO> {
    return await this.userService.register(usersInput);
  }

  @Delete('logout')
  async logout(@GetToken() token: string): Promise<{ message: string }> {
    await this.userService.blacklist(token?.slice(7));
    return {
      message: 'You have been logged out!',
    };
  }
  @Get()
  // @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
  public async getAllUsers(@Query('name') name: string): Promise<UsersDTO[]> {
    const users = await this.userService.getAllUsers();

    if (name) {
      return users.filter(user => user.userName.toLowerCase().includes(name));
    }
    return users;
  }

  @Delete(':userId')
  @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
  public async deleteUser(
    @Param('userId') userId: number,
  ): Promise<{ msg: string }> {
    return await this.userService.deleteUser(userId);
  }


  @Post(':userId/avatar')
  @UseInterceptors(
    FileInterceptor('files', {
      storage: diskStorage({
        destination: './images/avatars',
        filename: (req, file, cb) => {
          // Generating a 32 random chars long string
          console.log('filename cb', file);
          const randomName = Array(32)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
          //Calling the callback passing the random name generated with the original extension name
          cb(null, `${randomName}${extname(file.originalname)}`);
        },
      }),
    }),
  )
  async uploadAvatar(@UploadedFile() files, @Param('id') id: string) {
    return await this.userService.uploadAvatar(Number(id), files.filename);
  }

  @Get(':id')
  //@UseGuards(BlacklistGuard)
  public async getById(@Param('id') id: number): Promise<UsersDTO> {
    const user = await this.userService.getById(id);

    if (user === undefined) {
      throw new NotFoundException(`No user with id ${id} was found`);
    }
    return user;
  }


}
