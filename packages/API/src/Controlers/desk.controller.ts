import { Meaning } from './../Enums/meaning.enums';
import { DesksDTO } from 'src/Dtos/desks.dto';
import { Controller, Get, Param, NotFoundException, UseGuards, Body, Delete, Post, ValidationPipe } from '@nestjs/common';
import { DeskService } from 'src/Services/desk.service';
import { BlacklistGuard } from 'src/Auth/blacklist.guard';
import { RolesGuard } from 'src/Auth/roles.guard';
import { UserRole } from 'src/Enums/users-role';

@Controller('desks')
export class DeskController {
    constructor(private readonly service: DeskService) { }

    @Get()
    public async getAll(): Promise<DesksDTO[]> {
        const desks = await this.service.getAll();
        return desks;
    }

    @Get(':id')
    public async getOne(@Param('id') id: number): Promise<DesksDTO> {
        const desks = await this.service.getOne(id);
        if (desks === undefined) {
            throw new NotFoundException(`No desk with id ${id}`);
        }
        return desks;
    }

    @Post()
    public async createDesk(@Body(new ValidationPipe({ whitelist: true }))country: DesksDTO): Promise<DesksDTO> {
        return await this.service.createDesk(country);
    }

    @Post(':id')
    // @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async updateDesk(
        @Param('id') deskId: number,
        @Body(new ValidationPipe({ whitelist: true })) desk: DesksDTO): Promise<DesksDTO> {
        return await this.service.updateDesk(deskId,desk);
    }
    @Post('/meaning/:id')
    // @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async updateMeanig(
        @Param('id') deskId: number,
        @Body(new ValidationPipe({ whitelist: true })) meaning: Meaning) {
        return await this.service.updateOnlyMeaning(deskId,meaning);
    }

    @Delete(':id')
    @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
    public async deleteDesk(
        @Param('id') deskId: number): Promise<{ msg: string }> {
        return await this.service.deleteDesk(deskId);
    }
}
