import { IsNotEmpty, IsString, Length } from 'class-validator';

export class CreateProjectDTO {

    id: number;

    @IsNotEmpty()
    @IsString()
    @Length(3, 100)
    name: string;

    @IsNotEmpty()
    @IsString()
    @Length(1, 1000)
    description: string;

}
