import { Controller, Param, UseGuards, Delete, Get, Query } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { BlacklistGuard } from '../Auth/blacklist.guard';
import { RolesGuard } from '../Auth/roles.guard';
import { UsersDTO } from '../Dtos/users.dto';
import { UserRole } from '../Enums/users-role';
import { AdminUsersService } from '../Services/admin-users.service';

@Controller('/admin/users')
@UseGuards(BlacklistGuard, AuthGuard('jwt'), new RolesGuard(UserRole.Admin))
export class AdminUsersController {

  constructor(private readonly adminUsersService: AdminUsersService) { }

  @Delete(':id')
  public async deleteUser(
    @Param('id') userId: number,
  ): Promise<{ msg: string }> {
    return await this.adminUsersService.deleteUser(userId);
  }

  @Get()
  public async getAllUsers(@Query('name') name: string): Promise<UsersDTO[]> {
    const users = await this.adminUsersService.getAllUsers();

    if (name) {
      return users.filter(user => user.userName.toLowerCase().includes(name));
    }
    return users;
  }
}