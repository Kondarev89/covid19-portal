import { Controller, Get, Param, NotFoundException, UseGuards, Body, Delete, Post, Put, ValidationPipe, UseInterceptors, UploadedFile } from '@nestjs/common';
import { RolesGuard } from '../Auth/roles.guard';
import { ProjectsDTO } from '../Dtos/projects.dto';
import { UserRole } from '../Enums/users-role';
import { ProjectService } from '../Services/projects.service';
import { BlacklistGuard } from '../Auth/blacklist.guard';
import { CreateProjectDTO } from './create-project.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { extname } from 'path';
import { diskStorage } from 'multer';
import { UsersDTO } from 'src/Dtos/users.dto';

@Controller('projects')
export class ProjectsController {
  constructor(private readonly service: ProjectService) { }

  @Get()
  public async allProjects(): Promise<ProjectsDTO[]> {
    const projects = await this.service.getAllProjects();
    return projects;
  }

  @Get(':id')
  // @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
  public async getById(@Param('id') id: number): Promise<ProjectsDTO> {
    const project = await this.service.findProjectById(id);

    if (project === undefined) {
      throw new NotFoundException(`No project with id ${id}`);
    }
    return project;
  }

  @Post()
  //@UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
  public async createProject(@Body(new ValidationPipe({ whitelist: true })) project: CreateProjectDTO): Promise<CreateProjectDTO> {
    return await this.service.createProject(project);
  }

  @Put(':id')
  @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
  public async updateProject(@Param('id') projectId: number, @Body(new ValidationPipe({ whitelist: true })) project: ProjectsDTO): Promise<ProjectsDTO> {
    return await this.service.updateProject(projectId, project);
  }

  @Delete(':id')
  @UseGuards(BlacklistGuard, new RolesGuard(UserRole.Admin))
  public async deleteProject(@Param('id') projectId: number): Promise<{ msg: string }> {
    return await this.service.deleteProject(projectId);
  }

  @Post(':id/cover')
  @UseInterceptors(
    FileInterceptor('files', {
      storage: diskStorage({
        destination: './images/covers',
        filename: (req, file, cb) => {
          // Generating a 32 random chars long string
          console.log('filename cb', file);
          const randomName = Array(32)
            .fill(null)
            .map(() => Math.round(Math.random() * 16).toString(16))
            .join('');
          //Calling the callback passing the random name generated with the original extension name
          cb(null, `${randomName}${extname(file.originalname)}`);
        },
      }),
    }),
  )
  async uploadCover(@Param('id') id: string, @UploadedFile() files) {
    return await this.service.uploadCover(Number(id), files.filename);
  }

  @Post('/:id/users/:userId')
  async assignProject(@Param('id') project: string, @Param('userId') user: string): Promise<UsersDTO> {
    return await this.service.assignProject(+project, +user);
  }
}
