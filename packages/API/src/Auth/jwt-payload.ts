export class JWTPayload {
  id: number;
  email: string;
  avatarPath: string;
  country: string;
  role: string; // map UserRole to string
}