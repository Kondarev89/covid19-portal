import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity('date')
export class DateEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    fromDate: Date;

    @Column()
    toDate: Date;
}