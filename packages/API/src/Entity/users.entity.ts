import { DeskEntity } from './desks.entity';
import { NextWeekEntity } from './nextweek.entity';
import { CurrentWeekEntity } from './currentweek.entity';
import { UserRole } from '../Enums/users-role';
import { Entity, PrimaryGeneratedColumn, Column, OneToOne, ManyToOne } from 'typeorm';
import { ProjectsEntity } from './projects.entity';

@Entity('users')
export class UsersEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('nvarchar', { length: 25 })
  firstName: string;

  @Column('nvarchar', { length: 25 })
  lastName: string;

  @Column('nvarchar', { length: 25 })
  userName: string;

  @Column('nvarchar', { length: 256 })
  password: string;

  @Column('nvarchar', { length: 256 })
  country: string;

  @Column('nvarchar', { length: 256 })
  email: string;

  @Column('nvarchar', { length: 100, default: "noAvatar.jpg" })
  avatarPath: string;

  @Column({ type: 'boolean', default: false })
  isDeleted: boolean;

  @Column({
    type: 'enum',
    enum: UserRole,
    default: UserRole.User,
  })
  role: UserRole;


  @ManyToOne(
    () => ProjectsEntity,

    project => project.users,
  )
  project: ProjectsEntity;

  @ManyToOne(
    () => CurrentWeekEntity,
    currentWeek => currentWeek.user,
  )
  currentWeek: CurrentWeekEntity;

  @ManyToOne(
    () => NextWeekEntity,
    nextWeek => nextWeek.user,
  )
  nextWeek: NextWeekEntity;


}