import { Entity, PrimaryGeneratedColumn, Column, OneToMany, } from 'typeorm';
import { UsersEntity } from './users.entity';

@Entity('projects')
export class ProjectsEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column('nvarchar', { length: 100 })
  name: string;

  @Column('nvarchar', { length: 5000})
  description: string;

  @OneToMany(
    () => UsersEntity,
    users => users.project,
  )
  users: UsersEntity[];

  @Column('nvarchar', { nullable: true })
  country: string;

  @Column({ type: 'boolean', default: false })
  isDone: boolean

  @Column('nvarchar', { length: 100, default: "noCover.jpg" })
  photoPath: string;
}
