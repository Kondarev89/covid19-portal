import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('floors')
export class FloorEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  maxDesks: number;

  @Column()
  distance: number;

  @Column({ nullable: true })
  country: string;

}
