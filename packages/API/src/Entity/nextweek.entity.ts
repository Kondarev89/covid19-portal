
import { Status } from 'src/Enums/status.enum';
import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { UsersEntity } from './users.entity';

@Entity('nextweek')
export class NextWeekEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ default: Status.Home })
    status: Status;

    @Column({ nullable: false, type: 'date', default: () => "CURRENT_TIMESTAMP" })
    fromDate: string;

    @Column({ nullable: false, type: 'date', default: () => "CURRENT_TIMESTAMP" })
    toDate: string;
    

    @OneToMany(
        () => UsersEntity,

        user => user.nextWeek,
    )
    user: UsersEntity;

}