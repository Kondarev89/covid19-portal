import { UsersEntity } from 'src/Entity/users.entity';
import { Column, Entity, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import { Status } from 'src/Enums/status.enum';

@Entity('currentweek')
export class CurrentWeekEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ default: Status.Home })
    status: Status;

    @Column({ nullable: false, type: 'date', default: () => "CURRENT_TIMESTAMP" })
    fromDate: string;

    @Column({ nullable: false, type: 'date', default: () => "CURRENT_TIMESTAMP" })
    toDate: string;
    
    @OneToMany(
        () => UsersEntity,

        user => user.currentWeek,
    )
    user: UsersEntity;

}