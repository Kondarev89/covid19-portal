import { Meaning } from './../Enums/meaning.enums';
import { Column, Entity, PrimaryGeneratedColumn, OneToOne } from 'typeorm';
import { UsersEntity } from './users.entity';
import { FloorParts } from 'src/Enums/floors-parts.enums';

@Entity('desks')
export class DeskEntity {
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ nullable: true })
    name: string;

    @Column('nvarchar', { length: 100, default: "noAvatar.jpg" })
    avatarPath: string;

    @Column({ default: Meaning.Available })
    meaning: Meaning;

    @Column({default: null})
    userId?: number;

    @Column({ nullable: true })
    country: string;
  
    @Column({ default: FloorParts.All })
    parts: FloorParts;
    
}
