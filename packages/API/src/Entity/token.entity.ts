import { Entity, PrimaryGeneratedColumn, Column, CreateDateColumn } from 'typeorm';

@Entity()
export class TokenEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({length: 255})
  token: string;

  @CreateDateColumn()
  blacklistedOn: Date;
}
