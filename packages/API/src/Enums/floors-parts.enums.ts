export enum FloorParts {
    All = 1,
    Branding = 2,
    Designing = 3,
    Photography= 4,
    Development = 5
}