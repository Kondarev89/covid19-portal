export enum Meaning {
    Available = 1,
    Occupied = 2,
    Forbidden = 3,
}