import { UsersDTO } from './../Dtos/users.dto';
import { UsersEntity } from 'src/Entity/users.entity';
import { NextWeekEntity } from './../Entity/NextWeek.entity';
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { TransformService } from './transform.service';
import { NextWeekDTO } from 'src/Dtos/NextWeek.dto';
import { JWTPayload } from 'src/Auth/jwt-payload';
@Injectable()
export class NextWeekService {

  constructor(
    private readonly service: TransformService,
    @InjectRepository(NextWeekEntity) private readonly nextWeekRepo: Repository<NextWeekEntity>,
    @InjectRepository(UsersEntity) private readonly userRepo: Repository<UsersEntity>,
  ) { }
  
  public async createNextWeek(currentWeek: NextWeekDTO,userid:number): Promise<NextWeekDTO> {
    const createCurrentWeek: NextWeekEntity = this.nextWeekRepo.create(currentWeek);
    createCurrentWeek.status = currentWeek.status;
    createCurrentWeek.fromDate = currentWeek.fromDate;
    createCurrentWeek.toDate = currentWeek.toDate;
    const createdCurrentWeek = await this.nextWeekRepo.save(createCurrentWeek);
    this.assignNextWeek(createdCurrentWeek.id,userid);
    return createdCurrentWeek;
  }

  async assignNextWeek(currentWeekId: number, userId: number): Promise<UsersDTO> {
    const currentWeekToАssign = await this.nextWeekRepo.findOne(currentWeekId, {
      relations: ['users', 'users.nextWeek']
    });
    const user = await this.userRepo.findOne(userId);
    user.currentWeek = currentWeekToАssign;
    await this.userRepo.save(user);
    return this.service.toUserDTO(user);
  }
}