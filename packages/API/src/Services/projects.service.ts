import { TransformService } from './transform.service';
import { BadGatewayException, Injectable, NotFoundException } from '@nestjs/common';
import { Repository } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { ProjectsDTO } from '../Dtos/projects.dto';
import { ProjectsEntity } from '../Entity/projects.entity';
import { CreateProjectDTO } from 'src/Controlers/create-project.dto';
import { UsersDTO } from 'src/Dtos/users.dto';
import { UsersEntity } from 'src/Entity/users.entity';

@Injectable()
export class ProjectService {
  private static readonly PROJECT_IS_NOT_FOUND_ERROR = `No project is found`;

  constructor(
    @InjectRepository(ProjectsEntity)
    private readonly projetRepository: Repository<ProjectsEntity>,

    @InjectRepository(UsersEntity)
    private readonly userRepository: Repository<UsersEntity>,

    private readonly transformService: TransformService,
  ) { }


  public async findProjectById(id: number): Promise<ProjectsEntity> {


    const project = await this.projetRepository.findOne(id, {
      relations: ['users', 'users.project']
    });

    if (!project) {
      throw new NotFoundException(ProjectService.PROJECT_IS_NOT_FOUND_ERROR);
    } else {
      return project;
    }
  }

  public async createProject(project: CreateProjectDTO): Promise<CreateProjectDTO> {
    const createProject: ProjectsEntity = this.projetRepository.create(project);
    createProject.name = project.name;
    createProject.description = project.description;
    const createdProject = await this.projetRepository.save(createProject);
    return createdProject;
  }

  public async updateProject(projectId: number, project: ProjectsDTO): Promise<ProjectsDTO> {
    const oldProject: ProjectsDTO = await this.findProjectById(projectId);
    oldProject.name = project.name;
    oldProject.description = project.description;
    await this.projetRepository.save(oldProject);
    return this.transformService.toProjectsDTO(
      await this.projetRepository.findOne({
        where: { id: projectId, isDone: false },
      }),
    );
  }

  public async deleteProject(projectId: number): Promise<{ msg: string }> {
    const projectForDelete: ProjectsEntity = await this.findProjectById(projectId);
    await this.projetRepository.save(projectForDelete);
    await this.projetRepository
      .createQueryBuilder()
      .update(ProjectsEntity)
      .where('id = :id', { id: projectId })
      .execute();

    return {
      msg: 'Project is done!',
    };
  }

  public async getAllProjects(): Promise<ProjectsDTO[]> {
    const projects = await this.projetRepository.find({
      where: { isDone: false },
      relations: ['users', 'users.project']
    });

    return projects;
  }

  public async returnOne(deskId: number): Promise<ProjectsDTO> {
    return this.transformService.toProjectsDTO(
      await this.projetRepository.findOne({
        where: { id: deskId },
      }),
    );
  }

  async uploadCover(id: number, filename: string) {

    const project = await this.findProjectById(id);

    project.photoPath = filename;
    await this.projetRepository.save(project);

    return this.transformService.toProjectsDTO(project);
  }

  async assignProject(projectId: number, userId: number): Promise<UsersDTO> {
    const projectToАssign = await this.projetRepository.findOne(projectId, {
      relations: ['users', 'users.project'],
      where: {
        isDone: false
      }
    });
    const user = await this.userRepository.findOne(userId);

    if (!user) {
      throw new NotFoundException('User not found');
    }

    if (!projectToАssign) {
      throw new NotFoundException('Project not found');
    }

    if (projectToАssign.users.find(u => u.id === user.id)) {
      throw new BadGatewayException('User already assigned to project');
    }

    user.project = projectToАssign;
    await this.userRepository.save(user);

    return this.transformService.toUserDTO(user);
  }

}
