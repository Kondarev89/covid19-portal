import { FloorEntity } from './../Entity/floor.entity';
import { NextWeekEntity } from './../Entity/nextweek.entity';
import { NextWeekDTO } from 'src/Dtos/NextWeek.dto';
import { CurrentWeekDTO } from 'src/Dtos/currentWeek.dto';
import { CurrentWeekEntity } from './../Entity/currentweek.entity';
import { CovidDTO } from './../Dtos/covid.dto';
import { CovidEntity } from './../Entity/covid.entity';
import { DeskEntity } from '../Entity/desks.entity';
import { DesksDTO } from '../Dtos/desks.dto';
import { FloorDTO } from '../Dtos/floor.dto';
import { ProjectsDTO } from '../Dtos/projects.dto';
import { ProjectsEntity } from '../Entity/projects.entity';
import { UsersDTO } from '../Dtos/users.dto';
import { UsersEntity } from '../Entity/users.entity';

export class TransformService {

    toDeskDTO(desk: DeskEntity): DesksDTO {
        return {
            id: desk.id,
            name: desk.name,
            country: desk.country,
            meaning: desk.meaning,
            userId: desk.userId,
            parts: desk.parts,
            avatarPath: desk.avatarPath
        };
    }


    public toFloorDTO(floor: FloorEntity): FloorDTO {
        return {
            id: floor.id,
            maxDesks: floor.maxDesks,
            distance: floor.distance,
            country: floor.country,
        };
    }
    public toCovidDTO(covid: CovidEntity): CovidDTO {
        return {
            id: covid.id,
            date: covid.date,
            country: covid.country,
            cases: covid.cases,
            todayCases: covid.todayCases,
            deaths: covid.deaths,
            todayDeaths: covid.todayDeaths,
            recovered: covid.recovered,
            active: covid.active,
            critical: covid.critical,
            casesPerOneMillion: covid.casesPerOneMillion,
            deathsPerOneMillion: covid.deathsPerOneMillion,
            totalTests: covid.totalTests,
            testsPerOneMillion: covid.testsPerOneMillion,
        }
    }
    toProjectsDTO(project: ProjectsEntity): ProjectsDTO {
        return {
            id: project.id,
            name: project.name,
            description: project.description,
            users: project.users,
            photoPath: project.photoPath,
            country: project.country,
            isDone: project.isDone,

        };
    }
    toUserDTO(user: UsersEntity): UsersDTO {
        return {
            id: user.id,
            firstName: user.firstName,
            lastName: user.lastName,
            userName: user.userName,
            email: user.email,
            country: user.country,
            avatarPath: user.avatarPath,
            project: user.project
        };
    }
    toCurrentWeekDTO(currentWeek: CurrentWeekEntity): CurrentWeekDTO {
        return {
            id: currentWeek.id,
            user: currentWeek.user ? currentWeek.user : null,
            status: currentWeek.status,
            fromDate: currentWeek.fromDate,
            toDate: currentWeek.toDate
        };
    }
    toNextWeekDTO(currentWeek: NextWeekEntity): NextWeekDTO {
        return {
            id: currentWeek.id,
            user: currentWeek.user ? currentWeek.user : null,
            status: currentWeek.status,
            fromDate: currentWeek.fromDate,
            toDate: currentWeek.toDate
        };
    }
}