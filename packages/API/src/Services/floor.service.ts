import { FloorEntity } from './../Entity/floor.entity';
import { TransformService } from './transform.service';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { FloorDTO } from '../Dtos/floor.dto';

@Injectable()
export class FloorService {

    constructor(@InjectRepository(FloorEntity) private readonly repository: Repository<FloorEntity>, private readonly service: TransformService) { }

    public async createOne(floor: FloorDTO): Promise<FloorDTO> {
        const createFloor: FloorEntity = this.repository.create(floor);
        // desks should be connected with parts 
        createFloor.maxDesks = floor.maxDesks;
        createFloor.distance = floor.distance;
        createFloor.country = floor.country;
        const createdFloors = await this.repository.save(createFloor);
        return createdFloors;
    }

    public async updateOne(floorsId: number, floor: FloorDTO): Promise<FloorDTO> {
        const oldFloor: FloorDTO = await this.getOne(floorsId);
        oldFloor.maxDesks = floor.maxDesks;
        oldFloor.distance = floor.distance;
        await this.repository.save(oldFloor);
        return this.service.toFloorDTO(
            await this.repository.findOne({
                where: { id: floorsId },
            }),
        );
    }

    public async deleteOne(floorId: number): Promise<{ msg: string }> {
        const floorForDelete: FloorEntity = await this.repository.findOne(floorId);
        await this.repository.save(floorForDelete);
        await this.repository
            .createQueryBuilder()
            .update(FloorEntity)
            .where('id = :id', { id: floorId })
            .execute();

        return {
            msg: 'Floor is done!',
        };
    }
    async getOne(id: number): Promise<FloorDTO> {
        return this.service.toFloorDTO(await this.repository.findOne(id));
    }

    public async findAll(): Promise<FloorDTO[]> {
        const projects = await this.repository.find();
        return projects.map(e => this.service.toFloorDTO(e));
      }

}