import { Injectable, NotFoundException, ForbiddenException, UnauthorizedException, BadGatewayException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { JWTPayload } from '../Auth/jwt-payload';
import { UsersRegisterDTO } from '../Dtos/users-register.dto';
import { UsersLoginDTO } from '../Dtos/users-login.dto';
import { UsersDTO } from '../Dtos/users.dto';
import { TokenEntity } from '../Entity/token.entity';
import { UsersEntity } from '../Entity/users.entity';
import { UserRole } from '../Enums/users-role';
import { TransformService } from './transform.service';
import { ProjectsEntity } from 'src/Entity/projects.entity';

@Injectable()
export class UsersService {
  private static readonly USER_DUPLICATED_ERROR = `User already exist`;
  private static readonly USER_DOESNT_EXIST_ERROR = `User doesn't exist`;
  private static readonly USER_NOT_FOUND_ERROR = `No user found`;
  private static readonly USER_WRONG_CREDENTIALS_ERROR = `Wrong credentials!`;

  constructor(
    @InjectRepository(UsersEntity)
    private readonly usersRepository: Repository<UsersEntity>,

    @InjectRepository(ProjectsEntity)
    private readonly projectsRepository: Repository<ProjectsEntity>,

    @InjectRepository(TokenEntity)
    private readonly tokenRepository: Repository<TokenEntity>,
    private readonly userTransformService: TransformService,
    private readonly jwtService: JwtService,
  ) { }

  async register(userInput: UsersRegisterDTO): Promise<UsersDTO> {
    await this.doesUserExist(userInput.email);
    const user = this.usersRepository.create(userInput);
    // hash the user's password
    user.password = await bcrypt.hash(user.password, 10);

    const created = await this.usersRepository.save(user);

    return this.userTransformService.toUserDTO(created);
  }

  public async login(loginUser: UsersLoginDTO): Promise<string> {
    const user = await this.validateUser(
      loginUser.email,
      loginUser.password,
    );

    if (!user) {
      throw new UnauthorizedException(
        UsersService.USER_WRONG_CREDENTIALS_ERROR,
      );
    }

    const payload: JWTPayload = {
      id: user.id,
      email: user.email,
      avatarPath: user.avatarPath,
      country: user.country,
      role: UserRole[user.role],
    };

    const token = await this.jwtService.signAsync(payload);
    return token; // this will be returned to the client on successful login
  }

  public async validateUser(
    email: string,
    password: string,
  ): Promise<UsersEntity> {
    const user = await this.findUserByName(email);
    if (!user) {
      return null;
    }
    const isUserValidated = await bcrypt.compare(password, user.password);
    return isUserValidated ? user : null;
  }

  public async doesUserExist(email: string): Promise<UsersEntity> {
    const foundUser: UsersEntity = await this.usersRepository.findOne({
      email: email,
    });
    if (foundUser !== undefined) {
      throw new ForbiddenException(UsersService.USER_DUPLICATED_ERROR);
    }
    return foundUser;
  }

  public async findUserByName(email: string): Promise<UsersEntity> {
    const foundUser: UsersEntity = await this.usersRepository.findOne({
      email,
      isDeleted: false,
    });
    if (foundUser === undefined) {
      throw new ForbiddenException(UsersService.USER_DOESNT_EXIST_ERROR);
    }
    return foundUser;
  }

  public async findUserById(userId: number): Promise<UsersEntity> {
    const foundUser: UsersEntity = await this.usersRepository.findOne({
      id: userId,
    });
    if (foundUser === undefined) {
      throw new NotFoundException(UsersService.USER_NOT_FOUND_ERROR);
    }
    return foundUser;
  }

  async isBlacklisted(token: string): Promise<boolean> {
    return Boolean(await this.tokenRepository.findOne({
      where: { token },
    }),
    );
  }

  async blacklist(token: string): Promise<void> {
    const tokenEntity = this.tokenRepository.create();
    tokenEntity.token = token;

    await this.tokenRepository.save(tokenEntity);
  }

  public async deleteUser(userId: number): Promise<{ msg: string }> {
    const userDel = await this.findUserById(userId);

    userDel.isDeleted = true;

    await this.usersRepository.save(userDel);

    await this.usersRepository
      .createQueryBuilder()
      .update(UsersEntity)
      .set({ isDeleted: true })
      .where('id = :id', { id: userId })
      .execute();

    return {
      msg: 'User has been deleted!',
    };
  }

  public async getAllUsers(): Promise<UsersDTO[]> {
    const users = await this.usersRepository.find({ relations: ['project'] });

    return users.map(e => this.userTransformService.toUserDTO(e));
  }


  async getById(id: number): Promise<UsersDTO> {
    return this.userTransformService.toUserDTO(
      await this.findUserById(id),
    );
  }


  async uploadAvatar(id: number, filename: string) {
    const user = await this.findUserById(id);

    user.avatarPath = filename;
    await this.usersRepository.save(user);

    return this.userTransformService.toUserDTO(user);
  }
}
