import { CovidDTO } from './../Dtos/covid.dto';
import { CovidEntity } from './../Entity/covid.entity';
import { TransformService } from './transform.service';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class CovidService {
    constructor(
        @InjectRepository(CovidEntity)
        private readonly covidRepository: Repository<CovidEntity>,
        private readonly covidTransformService: TransformService,
    ) { }

    async getById(id: number): Promise<CovidDTO> {
        return this.covidTransformService.toCovidDTO(await this.findCovidById(id));
    }

    public async findCovidById(id: number): Promise<CovidDTO> {
        const element = await this.covidRepository.findOne({ id });

        if (!element) {
            throw new NotFoundException("Element does not exist");
        } else {
            return element;
        }
    }

    public async createCovid(covid: CovidDTO, test: CovidDTO): Promise<CovidDTO> {
        const createCovid: CovidEntity = this.covidRepository.create(covid);
        createCovid.id = test.id;
        createCovid.date = test.date;
        createCovid.country = covid.country;
        createCovid.cases = covid.cases;
        createCovid.todayCases = covid.todayCases;
        createCovid.deaths = covid.deaths;
        createCovid.todayDeaths = covid.todayDeaths;
        createCovid.recovered = covid.recovered;
        createCovid.active = covid.active;
        createCovid.critical = covid.critical;
        createCovid.casesPerOneMillion = covid.casesPerOneMillion;
        createCovid.deathsPerOneMillion = covid.deathsPerOneMillion;
        createCovid.totalTests = covid.totalTests;
        createCovid.testsPerOneMillion = covid.testsPerOneMillion;

        const createdCovid = await this.covidRepository.save(createCovid);

        return createdCovid;

    }

    public async updateCovid(covidId: number, covid: CovidDTO): Promise<CovidDTO> {
        const oldCovid: CovidDTO = await this.getById(covidId);
        oldCovid.country = covid.country;
        oldCovid.cases = covid.cases;
        oldCovid.todayCases = covid.todayCases;
        oldCovid.deaths = covid.deaths;
        oldCovid.todayDeaths = covid.todayDeaths;
        oldCovid.recovered = covid.recovered;
        oldCovid.active = covid.active;
        oldCovid.critical = covid.critical;
        oldCovid.casesPerOneMillion = covid.casesPerOneMillion;
        oldCovid.deathsPerOneMillion = covid.deathsPerOneMillion;
        oldCovid.totalTests = covid.totalTests;
        oldCovid.testsPerOneMillion = covid.testsPerOneMillion;
        await this.covidRepository.save(oldCovid);

        return this.covidTransformService.toCovidDTO(
            await this.covidRepository.findOne({
                where: { id: covidId },
            }),
        );
    }

    public async deteleCovid(covidId: number): Promise<{ msg: string }> {
        const covidForDelete: CovidEntity = await this.findCovidById(covidId);
        await this.covidRepository.save(covidForDelete);
        await this.covidRepository
            .createQueryBuilder()
            .update(CovidEntity)
            .where('id = :id', { id: covidId })
            //   .set({ isDone: true })
            .execute();

        return {
            msg: 'Covid is deleted!',
        };
    }

    async getAll(): Promise<CovidDTO[]> {
        const covid: CovidDTO[] = await this.covidRepository.find();
        return covid.map(cov => this.covidTransformService.toCovidDTO(cov));
    }
    public async returnOne(deskId: number): Promise<CovidDTO> {
        return this.covidTransformService.toCovidDTO(
            await this.covidRepository.findOne({
                where: { id: deskId },
            }),
        );
    }

}
