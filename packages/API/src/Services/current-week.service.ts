import { UsersEntity } from 'src/Entity/users.entity';
import { CurrentWeekEntity } from './../Entity/currentweek.entity';
import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { Repository } from "typeorm";
import { TransformService } from './transform.service';
import { CurrentWeekDTO } from 'src/Dtos/currentWeek.dto';
import { JWTPayload } from 'src/Auth/jwt-payload';
import { UsersDTO } from 'src/Dtos/users.dto';

@Injectable()
export class CurrentWeekService {
  constructor(
    private readonly service: TransformService,
    @InjectRepository(CurrentWeekEntity) private readonly currentWeekRepo: Repository<CurrentWeekEntity>,
    @InjectRepository(UsersEntity) private readonly userRepo: Repository<UsersEntity>,
  ) { }
  
  public async createCurrentWeek(currentWeek: CurrentWeekDTO,userid:number): Promise<CurrentWeekDTO> {
    const createCurrentWeek: CurrentWeekEntity = this.currentWeekRepo.create(currentWeek);
    createCurrentWeek.status = currentWeek.status;
    createCurrentWeek.fromDate = currentWeek.fromDate;
    createCurrentWeek.toDate = currentWeek.toDate;
    const createdCurrentWeek = await this.currentWeekRepo.save(createCurrentWeek);
    this.assignCurrentWeek(createdCurrentWeek.id,userid);
    return createdCurrentWeek;
  }

  async assignCurrentWeek(currentWeekId: number, userId: number): Promise<UsersDTO> {
    const currentWeekToАssign = await this.currentWeekRepo.findOne(currentWeekId, {
      relations: ['users', 'users.currentWeek']
    });
    const user = await this.userRepo.findOne(userId);
    user.currentWeek = currentWeekToАssign;
    await this.userRepo.save(user);
    return this.service.toUserDTO(user);
  }
}