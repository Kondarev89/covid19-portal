
import { UsersDTO } from './../Dtos/users.dto';
import { TransformService } from './transform.service';
import { UsersService } from './users.service';
import { UsersEntity } from './../Entity/users.entity';
import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';



@Injectable()
export class AdminUsersService {
  constructor(
    @InjectRepository(UsersEntity) private readonly usersRepository: Repository<UsersEntity>,
    private readonly usersService: UsersService,
    private readonly usersTransformService: TransformService,
  ) { }

  public async deleteUser(userId: number): Promise<{ msg: string }> {
    const userDel = await this.usersService.findUserById(userId);

    userDel.isDeleted = true;

    await this.usersRepository.save(userDel);

    await this.usersRepository
      .createQueryBuilder()
      .update(UsersEntity)
      .set({ isDeleted: true })
      .where('id = :id', { id: userId })
      .execute();

    return {
      msg: 'User has been deleted!',
    };
  }

  public async getAllUsers(): Promise<UsersDTO[]> {
    const users = await this.usersRepository.find({ isDeleted: false });

    return users.map(e => this.usersTransformService.toUserDTO(e));
  }
}