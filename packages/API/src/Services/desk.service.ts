import { Meaning } from './../Enums/meaning.enums';
import { UsersDTO } from 'src/Dtos/users.dto';
import { DesksDTO } from '../Dtos/desks.dto';
import { DeskEntity } from './../Entity/desks.entity';
import { TransformService } from './transform.service';
import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';

@Injectable()
export class DeskService {
  constructor(@InjectRepository(DeskEntity) private readonly repository: Repository<DeskEntity>, private readonly service: TransformService) { }

  async getOne(id: number): Promise<DesksDTO> {
    return this.service.toDeskDTO(await this.findLocationById(id));
  }

  public async findLocationById(id: number): Promise<DesksDTO> {
    const desk = await this.repository.findOne({ id });
    if (!desk) {
      throw new NotFoundException("Desk Does not exist");
    } else {
      return desk;
    }
  }

  public async createDesk(desk: DesksDTO): Promise<DesksDTO> {
    const createDesk: DeskEntity = this.repository.create({
      name: desk.name,
      parts: desk.parts,
      meaning: desk.meaning,
      country: desk.country,
      avatarPath: desk.avatarPath
    });
    return await this.repository.save(createDesk);
  }

  public async updateDesk(deskId: number,desk: DesksDTO): Promise<DesksDTO> {
    const oldDesk: DesksDTO = await this.getOne(deskId);
    oldDesk.meaning = desk.meaning;
    oldDesk.userId = desk.userId;
    return await this.repository.save(oldDesk);
  }
  public async updateOnlyMeaning(deskId: number, meaning: Meaning): Promise<DesksDTO> {
    const oldDesk: DesksDTO = await this.getOne(deskId);
    oldDesk.meaning = meaning;
    return await this.repository.save(oldDesk);
  }


  public async deleteDesk(deskId: number): Promise<{ msg: string }> {
    const deskForDelete: DesksDTO = await this.findLocationById(deskId);
    await this.repository.save(deskForDelete);
    await this.repository
      .createQueryBuilder()
      .update(DeskEntity)
      .where('id = :id', { id: deskId })
      .execute();

    return {
      msg: 'Desk is deleted!',
    };
  }
  async getAll(): Promise<DesksDTO[]> {
    const desks: DesksDTO[] = await this.repository.find();
    return desks.map(desk => this.service.toDeskDTO(desk));
  }

}
