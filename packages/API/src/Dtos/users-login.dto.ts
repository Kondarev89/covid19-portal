import { IsNotEmpty, Matches, IsEmail } from 'class-validator';
export class UsersLoginDTO {
    @IsEmail()
    @IsNotEmpty()
    email: string;

    @Matches(
        /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,15}$/,
        {
            message:
            'Your password must contain at least one number, one uppercase and lowercase letter, one spacial character and from 8 to 15 characters',
        },
    )
    password: string;
}