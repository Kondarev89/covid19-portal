import { UsersController } from './../Controlers/users.controller';
import { UsersDTO } from 'src/Dtos/users.dto';
import { IsNotEmpty, IsString, Length } from 'class-validator';

export class ProjectsDTO {
  id: number;

  @IsNotEmpty()
  @IsString()
  @Length(3, 100)
  name: string;

  @IsNotEmpty()
  @IsString()
  @Length(1, 1000)
  description: string;

  @IsNotEmpty()
  users: UsersDTO[];

  photoPath: string;

  @IsNotEmpty()
  @IsString()
  country: string;

  isDone: boolean;

}
