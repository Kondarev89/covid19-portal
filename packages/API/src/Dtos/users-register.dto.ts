import { IsString, IsNotEmpty, Length, Matches, IsEmail } from 'class-validator';

export class UsersRegisterDTO {
  @IsString()
  @IsNotEmpty()
  @Length(3, 25)
  userName: string;

  @IsString()
  @IsNotEmpty()
  @Length(3, 25)
  firstName: string;

  @IsString()
  @IsNotEmpty()
  @Length(3, 25)
  lastName: string;

  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty()
  country: string;

  @Matches(
    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,15}$/,
    {
      message:
      'Your password must contain at least one number, one uppercase and lowercase letter, one spacial character and from 8 to 15 characters',
    },
  )
  password: string;
}
