import { UsersEntity } from 'src/Entity/users.entity';
import { Meaning } from './../Enums/meaning.enums';
import { IsEnum, IsNotEmpty, IsNumber, IsString } from 'class-validator';
import { FloorParts } from 'src/Enums/floors-parts.enums';

export class DesksDTO {
  id: number;

  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsString()
  country: string;

  user?: UsersEntity;

  @IsEnum(Meaning)
  meaning: Meaning;

  @IsEnum(FloorParts)
  parts: FloorParts;

  @IsNumber()
  userId?: number;
  
  @IsNotEmpty()
  @IsString()
  avatarPath: string;
}