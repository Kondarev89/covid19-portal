import { IsNotEmpty, IsString } from 'class-validator';
export class FloorDTO {
  id: number;

  @IsNotEmpty()
  maxDesks: number;

  @IsNotEmpty()
  distance: number;

  @IsNotEmpty()
  @IsString()
  country: string;

}
