import { ProjectsDTO } from './projects.dto';
import { ProjectsEntity } from '../Entity/projects.entity';
import { IsEmail, Length } from 'class-validator';
import { IsNotEmpty } from 'class-validator';
import { IsString } from 'class-validator';

export class UsersDTO {
  id: number;

  @IsString()
  @IsNotEmpty()
  @Length(3, 25)
  firstName: string;

  @IsString()
  @IsNotEmpty()
  @Length(3, 25)
  lastName: string;

  @IsString()
  @IsNotEmpty()
  @Length(3, 25)
  userName: string;

  @IsEmail()
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty()
  country: string;

  avatarPath: string;

  project: ProjectsDTO;
}
