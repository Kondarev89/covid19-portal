import {  IsDateString, IsEnum, IsNotEmpty } from "class-validator";
import { UsersEntity } from "src/Entity/users.entity";
import { Status } from "src/Enums/status.enum";

export class NextWeekDTO {
    id: number;

    @IsEnum(Status)
    status: Status;

    @IsNotEmpty()
    fromDate: string;
    
    @IsNotEmpty()
    toDate: string;  
    
    user: UsersEntity;

}