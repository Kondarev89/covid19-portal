import React, { useState } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import './css/materialdesignicons.min.css';
import './css/magnific-popup.css';
import './css/owl.carousel.min.css';
import './css/owl.theme.default.min.css';
import './css/animate.css';
import './css/animations-delay.css';
import './css/style.css';
import './css/colors/default.css';
import { BrowserRouter, Switch, Route } from "react-router-dom";
import HomePage from './pages/HomePage';
import AboutPage from './pages/AboutPage';
import ServicePage from './pages/ServicePage';
import ProfilePage from './pages/ProfilePage';
import ErrorPage from './pages/ErrorPage';
import LoginPage from './pages/LoginPage';
import SingleProject from './pages/SingleCardPage';
import SignUpPage from './pages/SignUp.jsx';
import FloorPage from './pages/FloorPage';
import CasesPage from './pages/CasesPage';
import CaseDetailPage from './pages/CaseDetailPage';
import NavigationBar from './components/Home/NavigationBar';
import UsersPage from './pages/UsersPage';
import ProjectsPage from './pages/ProjectsPage';
import CreateFloorPage from './pages/CreateFloor';
import CreateProjectPage from './pages/CreateProject';
import CreateCurrentWeekPage from './pages/CreateCurrentWeek';
import UserContext, { getLoggedUser } from './Providers/userContext';
import ProtectedRoute from './Providers/ProtectedRoute';

export default function App() {
    const [user, setUser] = useState(getLoggedUser());

    return (<>
        <BrowserRouter>
            <UserContext.Provider value={{ user, setUser }}>
                <div className="App">
                    <NavigationBar />
                    <Switch>
                        <Route exact path='/' component={HomePage} />
                        <Route path="/home" component={HomePage} />
                        <Route path="/about" component={AboutPage} />
                        <Route path="/service" component={ServicePage} />
                        <Route path="/floor" component={FloorPage} />
                        <Route path="/projects/:id" component={SingleProject} />
                        <Route path="/cases" component={CasesPage} />
                        <ProtectedRoute user={user} path="/users" component={UsersPage} />
                        <ProtectedRoute user={user} path="/projects" component={ProjectsPage} />
                        <ProtectedRoute user={!user} path="/sign-in"  component={LoginPage} />
                        <ProtectedRoute user={!user} path="/sign-up" component={SignUpPage} />
                        <ProtectedRoute user={user}  path="/profile" component={ProfilePage} />
                        <ProtectedRoute user={user}  path="/createcurrentweek" component={CreateCurrentWeekPage} />
                        <ProtectedRoute user={user}  path="/nextweek" component={CreateCurrentWeekPage} />
                        <ProtectedRoute user={user}  path="/createproject" component={CreateProjectPage} />
                        <ProtectedRoute user={user}  path="/createfloor" component={CreateFloorPage} />                        

                        <Route path="*" component={ErrorPage} />
                    </Switch>
                </div>
            </UserContext.Provider>
        </BrowserRouter>
    </>);
}