import React from 'react';
import Position from '../components/About/Position';
import OurStory from '../components/About/OurStory';
import GreatestMind from '../components/About/GreatestMind';

export default function AboutPage() {
    return (<>
        <Position />
        <OurStory />
        <GreatestMind />
    </>);
}
