import React, { useState, useEffect } from 'react';
import ProjectDescriptionComponent from '../components/Project/ProjectDescription';
import ProjectImagesComponent from '../components/Project/ProjectImages';
import { BASE_URL } from '../Requests/requests';
import { NavLink } from 'react-router-dom';


const SingleProject = ({ match }) => {

    const { params: { id } } = match;
    const [project, setProject] = useState([]);
    const [users, setUsers] = useState([{ email: '' }]);
    const updateUsers = users => setProject({ ...project, users });

    useEffect(() => {
        fetch(`${BASE_URL}/projects/${id}`,
            {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
                }
            })
            .then(response => response.json())
            .then(result => {
                if (result) {
                    setProject(result);
                }
            }).catch(err => console.log(err.message));
    }, []);

    const assignUser = (userId) => {
        fetch(`${BASE_URL}/projects/${id}/users/${userId}`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
            }
        }).then(r => r.json())
            .then(r => {
                if (r.error) {
                    alert(r.message)
                } else {
                    setUsers(true)
                }
            })
    }

    return (
        <>
            <section className="bg-half bg-light d-table w-100">
                <div className="container">
                    <div className="row justify-content-center">
                        <div className="col-lg-12 text-center">
                            <div className="page-next-level">
                                <h4 className="title"> {project.name} </h4>
                                <div className="page-next">
                                    <nav aria-label="breadcrumb" className="d-inline-block">
                                        <ul className="breadcrumb bg-white rounded shadow mb-0">
                                            <li className="breadcrumb-item"><NavLink to="#">Page</NavLink></li>
                                            <li className="breadcrumb-item"><NavLink to="#">Project</NavLink></li>
                                            <li className="breadcrumb-item active" aria-current="page">Project Detail</li>
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section>
                <div className="position-relative">
                    <div className="shape overflow-hidden text-white">
                        <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
                        </svg>
                    </div>
                </div>
                <div className="bg-light rounded p-4">
                    <p className="text-muted font-italic mb-0">{project.description}</p>
                </div>
            </section>
            <section>
                <div>
                    Assigned to:
                
                    {project.users ? (project.users ? project.users.map(u => u.email) : null) : null}
                </div>
            </section>
            <ProjectImagesComponent />

            {/* <section>
                <div className="col-md-8">
                    <div className="form-group position-relative">
                        <label>Assign emplyees: <span className="text-danger">*</span></label>
                        <input id="assign" type="text" className="form-control pl-5" placeholder="Assign employee" required="" onChange={e => updateUsers(e.target.value)} />
                        <button className="btn btn-primary btn-block" onClick={() => assignUser()}>Send</button>
                    </div>
                </div>
            </section> */}
        </>
    );
}

export default SingleProject;