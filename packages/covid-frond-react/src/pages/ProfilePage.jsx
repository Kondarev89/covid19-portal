import React from 'react';
import ProfileBanner from '../components/Profile/ProfileBanner';
import ProfileInfoComponent from '../components/Profile/ProfileInfoComponent';
import * as Icon from 'react-feather';



export default function ProfilePage() {
    return (<>
       <section className="bg-profile d-table w-100 bg-primary ProfileBegginingImage"
        >
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card public-profile border-0 rounded shadow ProfileBeggining">
                            <div className="card-body">
                                <div className="row align-items-center">
                                    <div className="col-lg-2 col-md-3 text-md-left text-center">
                                    <img src="https://lh3.googleusercontent.com/-2HQ6NiScjLU/X1FgVFXU8yI/AAAAAAAABeA/HX5s1TzGJOcP8QsZZ2A8tjs-Rke5txxNQCEwYBhgLKtQDAL1Ocqz0yLJUKnV1QvUCoA7UfJs6O_NtnSH59pSaA935JwOZpLCQRGqUz1KZfFAru4mG_Kde8O04iEUQCquVtiyljpGMPn_-wOiSfU5HKC5TKM0aelULp07dCqBayv3oK4XdkrzrxeqdNASfB9J7xPxy32Sc5aUfM6-CIADD6bxbwMeVOqkaB8AUCulm2IZ-PQ5x8FFbGaUE_12Kdy1RO6Ex7TwNkQO5ZDnOO9a80xzNQvsoLIDVtGvlDnxkoI2WESL8WeZFL-SacStXswjX1cGrHV3VZAC89NkN6NsCMyA-RD0NpHJCtGzcQUvxcjZ_5BgPFJw_hJHZM6yAeEL6PTnxfmKHudJhc-v7hEbpzReL58OW56TqkBhH1F66GG6dNWmU4ZyVIDgjqfY2W7OG_QSr3R_pwE31SutSZZcD0pVHB1BVeeVVg1q_fAWOSn2GfiaZmYrIMzJnQJE3_nwF2Mtr-gDPVcYUMTic4LOPcbW_YhflIAejaCNqDCKDp4VL-nboz56S90ETad-wfaZaCOKg0Gqc1tSwf5yr5VnPYoftNaf5BKjQqbWVVv-2WXlp13XiwNDZfKnFEGzyBj-nTMusajanhjVdfc6USknqAxrA8JvZMOD3s_sF/w439-h440-p/ProfilePicture.JPEG" className="img-fluid avatar avatar-ex-large rounded-circle" alt="" />

                                    </div>

                                    <div className="col-lg-10 col-md-9">
                                        <div className="row align-items-end">
                                            <div className="col-md-7 text-md-left text-center mt-4 mt-sm-0">
                                                <h3 className="title mb-0">Musa Leshkov</h3>
                                                <small className="text-muted h6 mr-2">Co-founder</small>
                                                <ul className="list-inline mb-0 mt-3">
                                                    <li className="list-inline-item mr-2"><a href="javascript:void(0)"
                                                        className="text-muted" title="Instagram"><Icon.Instagram data-feather="instagram"
                                                            className="fea icon-sm mr-2" />thisismusaa</a></li>
                                                    <li className="list-inline-item"><a href="javascript:void(0)"
                                                        className="text-muted" title="Linkedin"><Icon.Linkedin data-feather="linkedin"
                                                            className="fea icon-sm mr-2" />Musa Leshkov</a></li>
                                                </ul>
                                            </div>
                                            <div className="col-md-5 text-md-right text-center">
                                                <ul className="list-unstyled social-icon social mb-0 mt-4">
                                                    <li className="list-inline-item"><a href="javascript:void(0)"
                                                        className="rounded" data-toggle="tooltip" data-placement="bottom"
                                                        title="Add Friend"><Icon.UserPlus data-feather="user-plus"
                                                            className="fea icon-sm fea-social" /></a></li>
                                                    <li className="list-inline-item"><a href="javascript:void(0)"
                                                        className="rounded" data-toggle="tooltip" data-placement="bottom"
                                                        title="Messages"><Icon.MessageCircle data-feather="message-circle"
                                                            className="fea icon-sm fea-social" /></a></li>
                                                    <li className="list-inline-item"><a href="javascript:void(0)"
                                                        className="rounded" data-toggle="tooltip" data-placement="bottom"
                                                        title="Notifications"><Icon.Bell data-feather="bell"
                                                            className="fea icon-sm fea-social" /></a></li>
                                                    <li className="list-inline-item"><a href="account-setting.html"
                                                        className="rounded" data-toggle="tooltip" data-placement="bottom"
                                                        title="Settings"><Icon.Tool data-feather="tool"
                                                            className="fea icon-sm fea-social" /></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <section className="section mt-60">
            <div className="container mt-lg-3">
                <div className="row">
                    <div className="col-lg-4 col-md-6 col-12 d-lg-block d-none">
                        <div className="sidebar sticky-bar p-4 rounded shadow">
                            <div className="widget mt-4 pt-2">
                                <h5 className="widget-title">Projects :</h5>
                                <div className="progress-box mt-4">
                                    <h6 className="title text-muted">Progress</h6>
                                    <div className="progress">
                                        <div className="progress-bar position-relative bg-primary ProfileProgress" >
                                            <div className="progress-value d-block text-muted h6">24 / 48</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="widget">
                                <div className="row">
                                    <div className="col-6 mt-4 pt-2">
                                        <a href="account-profile.html"
                                            className="accounts active rounded d-block shadow text-center py-3">
                                            <span className="pro-icons h3 text-muted"><i className="uil uil-dashboard"></i></span>
                                            <h6 className="title text-dark h6 my-0">Profile</h6>
                                        </a>
                                    </div>

                                    <div className="col-6 mt-4 pt-2">
                                        <a href="account-members.html"
                                            className="accounts rounded d-block shadow text-center py-3">
                                            <span className="pro-icons h3 text-muted"><i className="uil uil-users-alt"></i></span>
                                            <h6 className="title text-dark h6 my-0">Members</h6>
                                        </a>
                                    </div>

                                    <div className="col-6 mt-4 pt-2">
                                        <a href="account-works.html"
                                            className="accounts rounded d-block shadow text-center py-3">
                                            <span className="pro-icons h3 text-muted"><i className="uil uil-file"></i></span>
                                            <h6 className="title text-dark h6 my-0">Works</h6>
                                        </a>
                                    </div>

                                    <div className="col-6 mt-4 pt-2">
                                        <a href="account-messages.html"
                                            className="accounts rounded d-block shadow text-center py-3">
                                            <span className="pro-icons h3 text-muted"><i
                                                className="uil uil-envelope-star"></i></span>
                                            <h6 className="title text-dark h6 my-0">Messages</h6>
                                        </a>
                                    </div>

                                    <div className="col-6 mt-4 pt-2">
                                        <a href="account-payments.html"
                                            className="accounts rounded d-block shadow text-center py-3">
                                            <span className="pro-icons h3 text-muted"><i className="uil uil-transaction"></i></span>
                                            <h6 className="title text-dark h6 my-0">Payments</h6>
                                        </a>
                                    </div>

                                    <div className="col-6 mt-4 pt-2">
                                        <a href="account-setting.html"
                                            className="accounts rounded d-block shadow text-center py-3">
                                            <span className="pro-icons h3 text-muted"><i className="uil uil-setting"></i></span>
                                            <h6 className="title text-dark h6 my-0">Settings</h6>
                                        </a>
                                    </div>

                                    <div className="col-6 mt-4 pt-2">
                                        <a href="auth-login-three.html"
                                            className="accounts rounded d-block shadow text-center py-3">
                                            <span className="pro-icons h3 text-muted"><i
                                                className="uil uil-sign-out-alt"></i></span>
                                            <h6 className="title text-dark h6 my-0">Logout</h6>
                                        </a>
                                    </div>
                                </div>
                            </div>

                            <div className="widget mt-4 pt-2">
                                <h5 className="widget-title">Follow me :</h5>
                                <ul className="list-unstyled social-icon mb-0 mt-4">
                                    <li className="list-inline-item"><a href="javascript:void(0)" className="rounded"><Icon.Facebook
                                        data-feather="facebook" className="fea icon-sm fea-social" /></a></li>
                                    <li className="list-inline-item"><a href="javascript:void(0)" className="rounded"><Icon.Instagram
                                        data-feather="instagram" className="fea icon-sm fea-social" /></a></li>
                                    <li className="list-inline-item"><a href="javascript:void(0)" className="rounded"><Icon.Twitter
                                        data-feather="twitter" className="fea icon-sm fea-social" /></a></li>
                                    <li className="list-inline-item"><a href="javascript:void(0)" className="rounded"><Icon.Linkedin
                                        data-feather="linkedin" className="fea icon-sm fea-social" /></a></li>
                                    <li className="list-inline-item"><a href="javascript:void(0)" className="rounded"><Icon.GitHub
                                        data-feather="github" className="fea icon-sm fea-social" /></a></li>
                                    <li className="list-inline-item"><a href="javascript:void(0)" className="rounded"><Icon.Youtube
                                        data-feather="youtube" className="fea icon-sm fea-social" /></a></li>
                                    <li className="list-inline-item"><a href="javascript:void(0)" className="rounded"><Icon.GitHub
                                        data-feather="gitlab" className="fea icon-sm fea-social" /></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-8 col-12">
                        <div className="border-bottom pb-4">
                            <h5>Krista Joseph</h5>
                            <p className="text-muted mb-0">I have started my career as a trainee and prove my self and achieve
                            all the milestone with good guidance and reach up to the project manager. In this journey, I
                            understand all the procedure which make me a good developer, team leader, and a project
                            manager.</p>
                        </div>

                        <div className="border-bottom pb-4">
                            <div className="row">
                                <div className="col-md-6 mt-4">
                                    <h5>Personal Details :</h5>
                                    <div className="mt-4">
                                        <div className="media align-items-center">
                                            <Icon.Mail data-feather="mail" className="fea icon-ex-md text-muted mr-3" />
                                            <div className="media-body">
                                                <h6 className="text-primary mb-0">Email :</h6>
                                                <a href="javascript:void(0)"
                                                    className="text-muted">kristajoseph0203@mail.com</a>
                                            </div>
                                        </div>
                                        <div className="media align-items-center mt-3">
                                            <Icon.Bookmark data-feather="bookmark" className="fea icon-ex-md text-muted mr-3" />
                                            <div className="media-body">
                                                <h6 className="text-primary mb-0">Skills :</h6>
                                                <a href="javascript:void(0)" className="text-muted">html</a>, <a
                                                    href="javascript:void(0)" className="text-muted">css</a>, <a
                                                        href="javascript:void(0)" className="text-muted">js</a>, <a
                                                            href="javascript:void(0)" className="text-muted">mysql</a>
                                            </div>
                                        </div>
                                        <div className="media align-items-center mt-3">
                                            <Icon.Italic data-feather="italic" className="fea icon-ex-md text-muted mr-3" />
                                            <div className="media-body">
                                                <h6 className="text-primary mb-0">Language :</h6>
                                                <a href="javascript:void(0)" className="text-muted">English</a>, <a
                                                    href="javascript:void(0)" className="text-muted">Japanese</a>, <a
                                                        href="javascript:void(0)" className="text-muted">Chinese</a>
                                            </div>
                                        </div>
                                        <div className="media align-items-center mt-3">
                                            <Icon.Globe data-feather="globe" className="fea icon-ex-md text-muted mr-3" />
                                            <div className="media-body">
                                                <h6 className="text-primary mb-0">Website :</h6>
                                                <a href="javascript:void(0)" className="text-muted">www.kristajoseph.com</a>
                                            </div>
                                        </div>
                                        <div className="media align-items-center mt-3">
                                            <Icon.Gift data-feather="gift" className="fea icon-ex-md text-muted mr-3" />
                                            <div className="media-body">
                                                <h6 className="text-primary mb-0">Birthday :</h6>
                                                <p className="text-muted mb-0">2nd March, 1996</p>
                                            </div>
                                        </div>
                                        <div className="media align-items-center mt-3">
                                            <Icon.MapPin data-feather="map-pin" className="fea icon-ex-md text-muted mr-3" />
                                            <div className="media-body">
                                                <h6 className="text-primary mb-0">Location :</h6>
                                                <a href="javascript:void(0)" className="text-muted">Beijing, China</a>
                                            </div>
                                        </div>
                                        <div className="media align-items-center mt-3">
                                            <Icon.Phone data-feather="phone" className="fea icon-ex-md text-muted mr-3" />
                                            <div className="media-body">
                                                <h6 className="text-primary mb-0">Cell No :</h6>
                                                <a href="javascript:void(0)" className="text-muted">(+12) 1254-56-4896</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div className="col-md-6 mt-4 pt-2 pt-sm-0">
                                    <h5>Experience :</h5>

                                    <div className="media key-feature align-items-center p-3 rounded shadow mt-4">
                                        <img src="http://shreethemes.in/landrick/layouts/images/job/Circleci.svg" className="avatar avatar-ex-sm" alt="" />
                                        <div className="media-body content ml-3">
                                            <h4 className="title mb-0">Senior Web Developer</h4>
                                            <p className="text-muted mb-0">3 Years Experience</p>
                                            <p className="text-muted mb-0"><a href="javascript:void(0)"
                                                className="text-primary">CircleCi</a> @London, UK</p>
                                        </div>
                                    </div>

                                    <div className="media key-feature align-items-center p-3 rounded shadow mt-4">
                                        <img src="http://shreethemes.in/landrick/layouts/images/job/Codepen.svg" className="avatar avatar-ex-sm" alt="" />
                                        <div className="media-body content ml-3">
                                            <h4 className="title mb-0">Web Designer</h4>
                                            <p className="text-muted mb-0">2 Years Experience</p>
                                            <p className="text-muted mb-0"><a href="javascript:void(0)"
                                                className="text-primary">Codepen</a> @Washington D.C, USA</p>
                                        </div>
                                    </div>

                                    <div className="media key-feature align-items-center p-3 rounded shadow mt-4">
                                        <img src="http://shreethemes.in/landrick/layouts/images/job/Gitlab.svg" className="avatar avatar-ex-sm" alt="" />
                                        <div className="media-body content ml-3">
                                            <h4 className="title mb-0">UI Designer</h4>
                                            <p className="text-muted mb-0">2 Years Experience</p>
                                            <p className="text-muted mb-0"><a href="javascript:void(0)"
                                                className="text-primary">Gitlab</a> @Perth, Australia</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <h5 className="mt-4 mb-0">Posts & News :</h5>
                        <div className="row">
                            <div className="col-md-6 mt-4 pt-2">
                                <div className="card blog rounded border-0 shadow">
                                    <div className="position-relative">
                                        <img src="http://shreethemes.in/landrick/layouts/images/blog/01.jpg" className="card-img-top rounded-top" alt="..." />
                                        <div className="overlay rounded-top bg-dark"></div>
                                    </div>
                                    <div className="card-body content">
                                        <h5><a href="javascript:void(0)" className="card-title title text-dark">Design your apps
                                            in your own way</a></h5>
                                        <div className="post-meta d-flex justify-content-between mt-3">
                                            <ul className="list-unstyled mb-0">
                                                <li className="list-inline-item mr-2 mb-0"><a href="javascript:void(0)"
                                                    className="text-muted like"><i
                                                        className="mdi mdi-heart-outline mr-1"></i>33</a></li>
                                                <li className="list-inline-item"><a href="javascript:void(0)"
                                                    className="text-muted comments"><i
                                                        className="mdi mdi-comment-outline mr-1"></i>08</a></li>
                                            </ul>
                                            <a href="page-blog-detail.html" className="text-muted readmore">Read More <i
                                                className="mdi mdi-chevron-right"></i></a>
                                        </div>
                                    </div>
                                    <div className="author">
                                        <small className="text-light user d-block"><i className="mdi mdi-account"></i> Calvin
                                        Carlo</small>
                                        <small className="text-light date"><i className="mdi mdi-calendar-check"></i> 13th August,
                                        2019</small>
                                    </div>
                                </div>
                            </div>

                            <div className="col-md-6 mt-4 pt-2">
                                <div className="card blog rounded border-0 shadow">
                                    <div className="position-relative">
                                        <img src="http://shreethemes.in/landrick/layouts/images/blog/02.jpg" className="card-img-top rounded-top" alt="..." />
                                        <div className="overlay rounded-top bg-dark"></div>
                                    </div>
                                    <div className="card-body content">
                                        <h5><a href="javascript:void(0)" className="card-title title text-dark">How apps is
                                            changing the IT world</a></h5>
                                        <div className="post-meta d-flex justify-content-between mt-3">
                                            <ul className="list-unstyled mb-0">
                                                <li className="list-inline-item mr-2 mb-0"><a href="javascript:void(0)"
                                                    className="text-muted like"><i
                                                        className="mdi mdi-heart-outline mr-1"></i>33</a></li>
                                                <li className="list-inline-item"><a href="javascript:void(0)"
                                                    className="text-muted comments"><i
                                                        className="mdi mdi-comment-outline mr-1"></i>08</a></li>
                                            </ul>
                                            <a href="page-blog-detail.html" className="text-muted readmore">Read More <i
                                                className="mdi mdi-chevron-right"></i></a>
                                        </div>
                                    </div>
                                    <div className="author">
                                        <small className="text-light user d-block"><i className="mdi mdi-account"></i> Calvin
                                        Carlo</small>
                                        <small className="text-light date"><i className="mdi mdi-calendar-check"></i> 13th August,
                                        2019</small>
                                    </div>
                                </div>
                            </div>

                            <div className="col-12 mt-4 pt-2">
                                <a href="page-blog-grid.html" className="btn btn-primary">See More <i
                                    className="mdi mdi-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </>);
}
