import React from "react";
import ClientsReview from '../components/Services/ClientsReview';
import LatestProjects from '../components/Services/LatestProjects';
import SeeEvEmployee from '../components/Services/SeeEvEmployee';
import ServiceBanner from '../components/Services/ServiceBanner';
import WhatWeProvide from '../components/Services/WhatWeProvide';
export default function ServicePage() {
    return (<>

        <ServiceBanner />
        <section className="section">
            <WhatWeProvide />
            <ClientsReview />
        </section>
        <section className="section bg-light">
            <LatestProjects />
            <SeeEvEmployee />
        </section>
    </>);
}

