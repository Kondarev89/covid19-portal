import React, { useState, useEffect } from "react";
import { BASE_URL } from '../Requests/requests'


const UsersPage = () => {

    const [error, setError] = useState(null);
    const [user, setUser] = useState({
        userName: "",
        status: "",
        fromDate: "",
        toDate: "",
        project: "",
    });


    const url = `${BASE_URL}/currentWeek`;

    useEffect(() => {
        fetch(url,
            {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
                }
            })
            .then(result => {
                return result.json();
            })
            .then(result => {
                if (Array.isArray(result)) {
                    setUser(result);
                }
            })
            .catch(err => console.log(err.message));
    }, [url]);


    console.log(user)
    return (
        <>
            <div className="container py-5">

                <div className="row py-5">
                    <div className="col-lg-10 mx-auto">
                        <div className="card rounded shadow border-0">
                            <div className="card-body p-5 bg-white rounded">
                                <div className="table-responsive">
                                    <table id="example" className="tableWidth table table-striped table-bordered">
                                        <thead>
                                            <tr>
                                                <th>User Name</th>
                                                <th>User Status</th>
                                                <th>from date</th>
                                                <th>to date</th>
                                                <th>Project</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{user.userName}</td>
                                                <td>{user.status}</td>
                                                <td>{user.fromDate}</td>
                                                <td>{user.toDate}</td>
                                                <td>{user.project}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </>
    )
};


export default UsersPage;