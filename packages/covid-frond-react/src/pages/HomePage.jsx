import React from 'react';
import AboutOurComunity from '../components/Home/AboutOurCommunity';
import NavigationBar from '../components/Home/NavigationBar';
import TrustedClients from '../components/Home/TrustedClients';
import Banner from '../components/Home/Banner';
import Comfort from '../components/Home/Comfort';
import Dreamers from '../components/Home/Dreamers';

export default function HomePage() {
    return (<>
        <NavigationBar />
        <Banner />
        <TrustedClients />
        <AboutOurComunity />
        <Comfort />
        <Dreamers />
    </>);
}
