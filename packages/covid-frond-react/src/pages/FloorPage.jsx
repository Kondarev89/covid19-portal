import React, { useContext, useEffect, useState } from 'react';
import { GET_ALL_DESKS,GET_ALL_COVID,UPDATE_DESK_MEANING} from './../Requests/requests';
import FloorCardComponent from '../components/Floor/FloorCard';
import { Link } from 'react-router-dom';
import UserContext from '../Providers/userContext';

export default function FloorPage() {   
    const {user} = useContext(UserContext);
    const [desks, setDesks] = useState([]);      
    const [covids, setCovid] = useState([]);
    const [showBranding, toggleShowBranding] = useState(false);
    const [showDesigning, toggleShowDesigning] = useState(false);
    const [showPhotography, toggleShowPhotography] = useState(false);
    const [showDevelopment, toggleShowDevelopment] = useState(false); 
    const country = user.country;
    const BrandingEnum = 2;
    const DesigningEnum = 3;
    const PhotographyEnum = 4;
    const DevelopmentEnum = 5;
    useEffect(() => {
        GET_ALL_DESKS(setDesks)
        GET_ALL_COVID(setCovid)
    }, [])    
    const appDesk = { meaning: 3 }
    const updateDesk = (id) => {
        UPDATE_DESK_MEANING(id,appDesk.meaning)
    } 
    const FloorsDeskSortByCountry = desks.filter(desk => desk.country === country); 
    const covidCountryDate = covids.filter(covid => covid.country === country).reverse();
    let sevedDayCasesPerOneMillion = 0;
    let sevedDaytestsPerOneMillion= 0;

    for (let index = 0; index < covidCountryDate.length; index++) {
        sevedDayCasesPerOneMillion += covidCountryDate[index].casesPerOneMillion;
        sevedDaytestsPerOneMillion += covidCountryDate[index].testsPerOneMillion;
        }
    let percent = (sevedDayCasesPerOneMillion / sevedDaytestsPerOneMillion) * 100;
    if(percent > 10) {
        for (let index = 0; index < FloorsDeskSortByCountry.length; index++) {
            if(index % 2 === 0) updateDesk(FloorsDeskSortByCountry[index].id);
        }
    } else if(percent > 5 && percent < 10) {
        for (let index = 0; index < FloorsDeskSortByCountry.length; index++) {
            if(index % 3 === 0) updateDesk(FloorsDeskSortByCountry[index].id);
        }    
    } 
    const FloorsDeskSortByBranding = FloorsDeskSortByCountry.filter(desk => desk.parts === BrandingEnum);
    const FloorsDeskSortByDesigning = FloorsDeskSortByCountry.filter(desk => desk.parts === DesigningEnum);
    const FloorsDeskSortByPhotography = FloorsDeskSortByCountry.filter(desk => desk.parts === PhotographyEnum);
    const FloorsDeskSortByDevelopment = FloorsDeskSortByCountry.filter(desk => desk.parts === DevelopmentEnum);
    let today = new Date(),date = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();

    return (<>
        <section className="section">
            <div className="container">
            <div className="row">
                <ul className="col container-filter list-unstyled categories-filter text-center mb-0" id="filter">
                    <li className="list-inline-item">
                        <Link className="categories border d-block text-dark rounded"  onClick={() => toggleShowBranding(!showBranding)}>Branding</Link>
                    </li>
                    <li className="list-inline-item">
                        <Link className="categories border d-block text-dark rounded"  onClick={() => toggleShowDesigning(!showDesigning)}>Designing</Link>
                    </li>
                    <li className="list-inline-item">
                        <Link className="categories border d-block text-dark rounded"  onClick={() => toggleShowPhotography(!showPhotography)}>Photography</Link>
                    </li>
                    <li className="list-inline-item">
                        <Link className="categories border d-block text-dark rounded"  onClick={() => toggleShowDevelopment(!showDevelopment)}>Development</Link>
                    </li>
                </ul>
            </div>
                <div className="row projects-wrapper mt-4 pt-2">
                    {/* {showAll ? null:
                        <>  
                            {FloorsDeskSortByCountry.map(desk => {
                                return(
                                <FloorCardComponent 
                                    key={desk.id}
                                    id={desk.id}
                                    Title="Abstract images"
                                    UnderTitle={desk.meaning === 1 ? 'Available' : desk.meaning  === 2 ? 'Occupied' : 'Forbidden'}
                                    UserName={desk.userId ? desk.userId : "Calvin Carlo"}
                                    Date={date}
                                    Hah={() => updateDesk(desk.id,desk.meaning)}
                                />)
                            })}
                        </>
                    }  */}
       
                    {showBranding ? null:
                        <>  
                            {FloorsDeskSortByBranding.map(desk => {                                 
                                return(
                                <FloorCardComponent 
                                    key={desk.id}
                                    id={desk.id}
                                    Title="Abstract images"
                                    UnderTitle={desk.meaning}
                                    UserName={desk.userId ? desk.userId : "You can sit here"}
                                    Date={date} 
                                />)
                            })}
                        </>
                    } 

                    {showDesigning ? null:
                        <>  
                            {FloorsDeskSortByDesigning.map(desk => {
                                return(
                                <FloorCardComponent 
                                    key={desk.id}
                                    id={desk.id}
                                    Title="Abstract images"
                                    UnderTitle={desk.meaning}
                                    UserName={desk.userId ? desk.userId : "You can sit here"}
                                    Date={date}
                                />)
                            })}
                        </>
                    } 

                    {showPhotography ? null:
                        <>  
                            {FloorsDeskSortByPhotography.map(desk => {
                                return(
                                <FloorCardComponent 
                                    key={desk.id}
                                    id={desk.id}
                                    Title="Abstract images"
                                    UnderTitle={desk.meaning}
                                    UserName={desk.userId ? desk.userId : "You can sit here"}
                                    Date={date}
                                />)
                            })}
                        </>
                    } 

                    {showDevelopment ? null:
                        <>  
                            {FloorsDeskSortByDevelopment.map(desk => {
                                return(
                                <FloorCardComponent 
                                    key={desk.id}
                                    id={desk.id}
                                    Title="Abstract images"
                                    UnderTitle={desk.meaning}
                                    UserName={desk.userId ? desk.userId : "You can sit here"}
                                    Date={date}
                                />)
                            })}
                        </>
                    } 

                </div>
            </div>
        </section>
    </>);
}
