import React from 'react';
import * as Icon from 'react-feather';
import SignUpComponent from '../components/Authentication/SignUpFormComponent';


export default function SignUpPage() {
    return (<>
        <div className="back-to-home rounded d-none d-sm-block">
            <a href="index.html" className="btn btn-icon btn-soft-primary"><Icon.Home data-feather="home" className="icons" /></a>
        </div>
        <section className="bg-home d-flex align-items-center">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-lg-7 col-md-6">
                        <div className="mr-lg-5">
                            <img src="http://shreethemes.in/landrick/layouts/images/user/signup.svg" className="img-fluid d-block mx-auto" alt="" />
                        </div>
                    </div>


                    <SignUpComponent />

                </div>
            </div>
        </section>
    </>);
}
