import React from 'react';
import LoginFormComponent from '../components/Authentication/LoginFormComponent';


export default function LoginPage() {
    return (<>

        <section className="bg-home d-flex align-items-center">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-lg-7 col-md-6">
                        <div className="mr-lg-5">
                            <img src="http://shreethemes.in/landrick/layouts/images/user/login.svg" className="img-fluid d-block mx-auto" alt="" />
                        </div>
                    </div>

                    <LoginFormComponent />

                </div>
            </div>
        </section>
    </>);
}
