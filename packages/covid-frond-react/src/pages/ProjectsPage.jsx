import React, { useEffect, useState } from 'react';
import { BASE_URL } from '../Requests/requests'
import ProjectCard from '.././components/Project/card-sample';

export default function ProjectsPage({ history }) {

    const [projects, setProjects] = useState([]);

    const url = `${BASE_URL}/projects`;

    useEffect(() => {
        fetch(url,
            {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
                }
            })
            .then(result => {
                return result.json();
            })
            .then(result => {
                if (Array.isArray(result)) {
                    setProjects(result);
                }
            })
            .catch(err => console.log(err.message));
    }, [url]);


    return (<>
        <div className="container py-5">

            <div className="row py-5">
                <div className="col mx-auto">
                    <div className="card rounded shadow border-0">
                        <div className="card-body p-5 bg-white rounded">
                            {projects
                                .map(project => <ProjectCard key={project.id} project={project} projectId={project.id} />
                                )}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </>)
}
