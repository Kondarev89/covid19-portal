import React,{useState, useEffect} from 'react';
import CasesBannerComponent from '../components/Cases/CasesBanner';
import CasesCardComponent from '../components/Cases/CasesCard';
import { GET_ALL_COVID } from '../Requests/requests';

export default function CasesPage() {
    const [covids, setCovid] = useState([]);
    useEffect(() => {
        GET_ALL_COVID(setCovid)
    }, [])  
    let today = new Date(),date = today.getFullYear() + '-0' + (today.getMonth() + 1) + '-' + today.getDate();    
    const filtered =  covids.filter(covid => covid.date === `${date}`)
 
   return (<>
        <CasesBannerComponent />
        <section className="section">
            <div className="container">
                <div className="row projects-wrapper">            
                {filtered.map(covid => {
                return(
                    <CasesCardComponent  
                    key={covid.id}
                    Cases= {covid.cases}
                    Country={covid.country}
                    />
                    )
                })}
                </div>
            </div>
        </section>
    </>);
}
