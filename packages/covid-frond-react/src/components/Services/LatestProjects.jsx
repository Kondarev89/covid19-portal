import React from 'react';
import { NavLink } from 'react-router-dom';

export default function LatestProjects() {
    return (<>

                <div className="col-md-6 col-12 mt-4 pt-2">
                    <div
                        className="card work-container work-modern position-relative overflow-hidden shadow rounded border-0">
                        <div className="card-body p-0">
                            <img src="http://shreethemes.in/landrick/layouts/images/work/1.jpg" className="img-fluid rounded" alt="aham" />
                            <div className="overlay-work bg-dark"></div>
                            <div className="content">
                                <NavLink to="page-work-detail.html"
                                    className="title text-white d-block font-weight-bold">Shifting Perspective</NavLink>
                                <small className="text-light">Studio</small>
                            </div>
                            <div className="client">
                                <small className="text-light user d-block"><i className="mdi mdi-account" /> Calvin
                                    Carlo</small>
                                <small className="text-light date"><i className="mdi mdi-calendar-check" /> 13th August,
                                    2019</small>
                            </div>
                        </div>
                    </div>
                </div>

        


    </>);
}
