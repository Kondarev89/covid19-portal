import React from 'react';
import { NavLink } from 'react-router-dom';

export default function SeeEvEmployee() {
    return (<>
        <div className="container mt-100 mt-60">
            <div className="row justify-content-center">
                <div className="col-12 text-center">
                    <div className="section-title">
                        <h4 className="title mb-4">See everything about your employee at one place.</h4>
                        <p className="text-muted para-desc mx-auto">Start working with <span
                            className="text-primary font-weight-bold">Landrick</span> that can provide everything you
                            need to generate awareness, drive traffic, connect.</p>

                        <div className="mt-4">
                            <NavLink to="" className="btn btn-primary mt-2 mr-2">Get Started Now</NavLink>
                            <NavLink to="" className="btn btn-outline-primary mt-2">Free Trial</NavLink>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </>);
}
