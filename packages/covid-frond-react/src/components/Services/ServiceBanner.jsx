import React from 'react';

export default function ServiceBanner() {
    return (<>
        <section className="bg-half bg-light d-table w-100">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-lg-12 text-center">
                        <div className="page-next-level">
                            <h4 className="title"> Services </h4>
                            <div className="page-next">
                                <nav aria-label="breadcrumb" className="d-inline-block">
                                    <ul className="breadcrumb bg-white rounded shadow mb-0">
                                        <li className="breadcrumb-item"><span>Landrick</span></li>
                                        <li className="breadcrumb-item"><span>Page</span></li>
                                        <li className="breadcrumb-item active" aria-current="page">Services</li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <div className="position-relative">
            <div className="shape overflow-hidden text-white">
                <svg viewBox="0 0 2880 48" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M0 48H1437.5H2880V0H2160C1442.5 52 720 0 720 0H0V48Z" fill="currentColor"></path>
                </svg>
            </div>
        </div>

    </>);
}
