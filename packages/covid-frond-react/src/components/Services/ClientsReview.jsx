import React from 'react';

export default function ClientsReview() {
    return (<>
        <div className="container mt-100 mt-60">
            <div className="row justify-content-center">
                <div className="col-12 text-center">
                    <div className="section-title mb-4 pb-2">
                        <h4 className="title mb-4">Client Reviews</h4>
                        <p className="text-muted para-desc mx-auto mb-0">Start working with <span
                            className="text-primary font-weight-bold">Landrick</span> that can provide everything you
                            need to generate awareness, drive traffic, connect.</p>
                    </div>
                </div>
            </div>

            <div className="row justify-content-center">
                <div className="col-lg-12 mt-4">
                    <div id="customer-testi" className="owl-carousel owl-theme">
                        <div className="media customer-testi m-2">
                            <img src="http://shreethemes.in/landrick/layouts/images/client/01.jpg" className="avatar avatar-small mr-3 rounded shadow" alt="" />
                            <div className="media-body content p-3 shadow rounded bg-white position-relative">
                                <ul className="list-unstyled mb-0">
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                </ul>
                                <p className="text-muted mt-2">" It seems that only fragments of the original text remain in
                                    the Lorem Ipsum texts used today. "</p>
                                <h6 className="text-primary">- Thomas Israel <small className="text-muted">C.E.O</small></h6>
                            </div>
                        </div>

                        <div className="media customer-testi m-2">
                            <img src="http://shreethemes.in/landrick/layouts/images/client/02.jpg" className="avatar avatar-small mr-3 rounded shadow" alt="" />
                            <div className="media-body content p-3 shadow rounded bg-white position-relative">
                                <ul className="list-unstyled mb-0">
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star-half text-warning" /></li>
                                </ul>
                                <p className="text-muted mt-2">" One disadvantage of Lorum Ipsum is that in Latin certain
                                    letters appear more frequently than others. "</p>
                                <h6 className="text-primary">- Barbara McIntosh <small className="text-muted">M.D</small></h6>
                            </div>
                        </div>

                        <div className="media customer-testi m-2">
                            <img src="http://shreethemes.in/landrick/layouts/images/client/03.jpg" className="avatar avatar-small mr-3 rounded shadow" alt="" />
                            <div className="media-body content p-3 shadow rounded bg-white position-relative">
                                <ul className="list-unstyled mb-0">
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                </ul>
                                <p className="text-muted mt-2">" The most well-known dummy text is the 'Lorem Ipsum', which
                                    is said to have originated in the 16th century. "</p>
                                <h6 className="text-primary">- Carl Oliver <small className="text-muted">P.A</small></h6>
                            </div>
                        </div>

                        <div className="media customer-testi m-2">
                            <img src="http://shreethemes.in/landrick/layouts/images/client/04.jpg" className="avatar avatar-small mr-3 rounded shadow" alt="" />
                            <div className="media-body content p-3 shadow rounded bg-white position-relative">
                                <ul className="list-unstyled mb-0">
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                </ul>
                                <p className="text-muted mt-2">" According to most sources, Lorum Ipsum can be traced back
                                    to a text composed by Cicero. "</p>
                                <h6 className="text-primary">- Christa Smith <small className="text-muted">Manager</small></h6>
                            </div>
                        </div>

                        <div className="media customer-testi m-2">
                            <img src="http://shreethemes.in/landrick/layouts/images/client/05.jpg" className="avatar avatar-small mr-3 rounded shadow" alt="" />
                            <div className="media-body content p-3 shadow rounded bg-white position-relative">
                                <ul className="list-unstyled mb-0">
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                </ul>
                                <p className="text-muted mt-2">" There is now an abundance of readable dummy texts. These
                                    are usually used when a text is required. "</p>
                                <h6 className="text-primary">- Dean Tolle <small className="text-muted">Developer</small></h6>
                            </div>
                        </div>

                        <div className="media customer-testi m-2">
                            <img src="http://shreethemes.in/landrick/layouts/images/client/06.jpg" className="avatar avatar-small mr-3 rounded shadow" alt="" />
                            <div className="media-body content p-3 shadow rounded bg-white position-relative">
                                <ul className="list-unstyled mb-0">
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                    <li className="list-inline-item"><i className="mdi mdi-star text-warning" /></li>
                                </ul>
                                <p className="text-muted mt-2">" Thus, Lorem Ipsum has only limited suitability as a visual
                                    filler for German texts. "</p>
                                <h6 className="text-primary">- Jill Webb <small className="text-muted">Designer</small></h6>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </>);
}
