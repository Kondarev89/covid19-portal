import React from 'react';
import * as Icon from 'react-feather';
import { NavLink } from 'react-router-dom';

export default function GreatestMind() {
    return (<>
        <section className="section bg-light">
            <div className="container">
                <div className="row">
                    <div className="col-12 text-center">
                        <div className="section-title mb-4 pb-2">
                            <h4 className="title mb-4">Our Greatest Minds</h4>
                            <p className="text-muted para-desc mx-auto mb-0">Start working with <span
                                className="text-primary font-weight-bold">WorkSpace</span> that can provide everything you
                            need to generate awareness, drive traffic, connect.</p>
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-3 col-md-6 mt-4 pt-2">
                        <div className="card team text-center bg-transparent border-0">
                            <div className="card-body p-0">
                                <div className="position-relative">
                                    <img src="https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRFrTquQ2FEHTmfn8LmnCVsr9Fzl26LDQ9tWQ&usqp=CAU" className="img-fluid avatar avatar-ex-large rounded-circle" alt="" />
                                    <ul className="list-unstyled social-icon team-icon mb-0 mt-4">
                                        <li className="list-inline-item">
                                            <NavLink to="https://www.facebook.com/" className="rounded">
                                                <Icon.Facebook data-feather="facebook" className="fea icon-sm fea-social" />
                                            </NavLink>
                                        </li>
                                        <li className="list-inline-item">
                                            <NavLink to="https://www.instagram.com/?hl=en" className="rounded">
                                                <Icon.Instagram data-feather="instagram" className="fea icon-sm fea-social" />
                                            </NavLink>
                                        </li>
                                        <li className="list-inline-item">
                                            <NavLink to="https://twitter.com/" className="rounded">
                                                <Icon.Twitter data-feather="twitter" className="fea icon-sm fea-social" />
                                            </NavLink>
                                        </li>
                                        <li className="list-inline-item">
                                            <NavLink to="https://www.linkedin.com/" className="rounded">
                                                <Icon.Linkedin data-feather="linkedin" className="fea icon-sm fea-social" />
                                            </NavLink>
                                        </li>
                                    </ul>
                                </div>
                                <div className="content pt-3 pb-3">
                                    <h5 className="mb-0"><span className="name text-dark">Stack OverFlow</span> </h5>
                                    <small className="designation text-muted">Co-founder</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-6 mt-4 pt-2">
                        <div className="card team text-center bg-transparent border-0">
                            <div className="card-body p-0">
                                <div className="position-relative">
                                    <img src="https://lh3.googleusercontent.com/-2HQ6NiScjLU/X1FgVFXU8yI/AAAAAAAABeA/HX5s1TzGJOcP8QsZZ2A8tjs-Rke5txxNQCEwYBhgLKtQDAL1Ocqz0yLJUKnV1QvUCoA7UfJs6O_NtnSH59pSaA935JwOZpLCQRGqUz1KZfFAru4mG_Kde8O04iEUQCquVtiyljpGMPn_-wOiSfU5HKC5TKM0aelULp07dCqBayv3oK4XdkrzrxeqdNASfB9J7xPxy32Sc5aUfM6-CIADD6bxbwMeVOqkaB8AUCulm2IZ-PQ5x8FFbGaUE_12Kdy1RO6Ex7TwNkQO5ZDnOO9a80xzNQvsoLIDVtGvlDnxkoI2WESL8WeZFL-SacStXswjX1cGrHV3VZAC89NkN6NsCMyA-RD0NpHJCtGzcQUvxcjZ_5BgPFJw_hJHZM6yAeEL6PTnxfmKHudJhc-v7hEbpzReL58OW56TqkBhH1F66GG6dNWmU4ZyVIDgjqfY2W7OG_QSr3R_pwE31SutSZZcD0pVHB1BVeeVVg1q_fAWOSn2GfiaZmYrIMzJnQJE3_nwF2Mtr-gDPVcYUMTic4LOPcbW_YhflIAejaCNqDCKDp4VL-nboz56S90ETad-wfaZaCOKg0Gqc1tSwf5yr5VnPYoftNaf5BKjQqbWVVv-2WXlp13XiwNDZfKnFEGzyBj-nTMusajanhjVdfc6USknqAxrA8JvZMOD3s_sF/w439-h440-p/ProfilePicture.JPEG" className="img-fluid avatar avatar-ex-large rounded-circle" alt="" />
                                    <ul className="list-unstyled social-icon team-icon mb-0 mt-4">
                                        <li className="list-inline-item">
                                            <NavLink to="https://www.facebook.com/" className="rounded">
                                                <Icon.Facebook data-feather="facebook" className="fea icon-sm fea-social" />
                                            </NavLink>
                                        </li>
                                        <li className="list-inline-item">
                                            <NavLink to="https://www.instagram.com/?hl=en" className="rounded">
                                                <Icon.Instagram data-feather="instagram" className="fea icon-sm fea-social" />
                                            </NavLink>
                                        </li>
                                        <li className="list-inline-item">
                                            <NavLink to="https://twitter.com/" className="rounded">
                                                <Icon.Twitter data-feather="twitter" className="fea icon-sm fea-social" />
                                            </NavLink>
                                        </li>
                                        <li className="list-inline-item">
                                            <NavLink to="https://www.linkedin.com/" className="rounded">
                                                <Icon.Linkedin data-feather="linkedin" className="fea icon-sm fea-social" />
                                            </NavLink>
                                        </li>
                                    </ul>
                                </div>
                                <div className="content pt-3 pb-3">
                                    <h5 className="mb-0"><span className="name text-dark">Musa Leshkov</span></h5>
                                    <small className="designation text-muted">Co-founder</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-6 mt-4 pt-2">
                        <div className="card team text-center bg-transparent border-0">
                            <div className="card-body p-0">
                                <div className="position-relative">
                                    <img src="https://scontent.fsof5-1.fna.fbcdn.net/v/t1.0-9/53320005_10218391834536434_4928410990032191488_o.jpg?_nc_cat=103&_nc_sid=174925&_nc_ohc=EpqkB0JnT3kAX82wutL&_nc_ht=scontent.fsof5-1.fna&oh=679a38dba5b8aa71ec53bdc804296d05&oe=5F90A05B" className="img-fluid avatar avatar-ex-large rounded-circle" alt="" />
                                    <ul className="list-unstyled social-icon team-icon mb-0 mt-4">
                                        <li className="list-inline-item">
                                            <NavLink to="https://www.facebook.com/" className="rounded">
                                                <Icon.Facebook data-feather="facebook" className="fea icon-sm fea-social" />
                                            </NavLink>
                                        </li>
                                        <li className="list-inline-item">
                                            <NavLink to="https://www.instagram.com/?hl=en" className="rounded">
                                                <Icon.Instagram data-feather="instagram" className="fea icon-sm fea-social" />
                                            </NavLink>
                                        </li>
                                        <li className="list-inline-item">
                                            <NavLink to="https://twitter.com/" className="rounded">
                                                <Icon.Twitter data-feather="twitter" className="fea icon-sm fea-social" />
                                            </NavLink>
                                        </li>
                                        <li className="list-inline-item">
                                            <NavLink to="https://www.linkedin.com/" className="rounded">
                                                <Icon.Linkedin data-feather="linkedin" className="fea icon-sm fea-social" />
                                            </NavLink>
                                        </li>
                                    </ul>
                                </div>
                                <div className="content pt-3 pb-3">
                                    <h5 className="mb-0"><span className="name text-dark">Aleksandrina Mankova</span> </h5>
                                    <small className="designation text-muted">Co-founder</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="col-lg-3 col-md-6 mt-4 pt-2">
                        <div className="card team text-center bg-transparent border-0">
                            <div className="card-body p-0">
                                <div className="position-relative">
                                    <img src="https://scontent.fsof5-1.fna.fbcdn.net/v/t1.0-9/116912352_10223682420236963_5344527998070727446_o.jpg?_nc_cat=106&_nc_sid=09cbfe&_nc_ohc=mh9kWece2uUAX8ZX3ph&_nc_ht=scontent.fsof5-1.fna&oh=ced597979995554f50e07415caa2a980&oe=5F92D89C" className="img-fluid avatar avatar-ex-large rounded-circle" alt="" />
                                    <ul className="list-unstyled social-icon team-icon mb-0 mt-4">
                                        <li className="list-inline-item"><NavLink to="https://www.facebook.com/" className="rounded"><Icon.Facebook
                                            data-feather="facebook" className="fea icon-sm fea-social" /></NavLink></li>
                                        <li className="list-inline-item"><NavLink to="https://www.instagram.com/?hl=en" className="rounded"><Icon.Instagram
                                            data-feather="instagram" className="fea icon-sm fea-social" /></NavLink></li>
                                        <li className="list-inline-item"><NavLink to="https://twitter.com/" className="rounded"><Icon.Twitter
                                            data-feather="twitter" className="fea icon-sm fea-social" /></NavLink></li>
                                        <li className="list-inline-item"><NavLink to="https://www.linkedin.com/" className="rounded"><Icon.Linkedin
                                            data-feather="linkedin" className="fea icon-sm fea-social" /></NavLink></li>
                                    </ul>
                                </div>
                                <div className="content pt-3 pb-3">
                                    <h5 className="mb-0"><span className="name text-dark">Tanyo Kondarev</span>  </h5>
                                    <small className="designation text-muted">Co-founder</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </>);
}
