import React from 'react';


export default function ProjectImagesComponent() {
    return (<>
        <div className="row">
            <div className="col-md-6 mt-4 pt-2">
                <img src="http://shreethemes.in/landrick/layouts/images/work/2.jpg" className="img-fluid rounded" alt="" />
            </div>

            <div className="col-md-6 mt-4 pt-2">
                <img src="http://shreethemes.in/landrick/layouts/images/work/3.jpg" className="img-fluid rounded" alt="" />
            </div>
        </div>
    </>)
}