import React, { useState } from 'react';
import { BASE_URL } from '../../Requests/requests';

export default function CreateProject(props) {


    const history = props.history;
    const [project, setProject] = useState({
        name: '',
        description: '',
        country: ''
    });
    const [files, setFiles] = useState([]);

    const updateName = name => setProject({ ...project, name });
    const updateDescription = description => setProject({ ...project, description });
    const updateCountry = country => setProject({ ...project, country });


    const uploadCover = (project) => {
        const formData = new FormData();
        if (!files.length) {
            alert('Project is created');
            return;
        }

        formData.append('files', files[0]);
        console.log(files[0])
        fetch(`${BASE_URL}/projects/${project.id}/cover`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
            },
            body: formData,
        })
            .then(r => r.json())
            .then(result => {
                if (result.error) {
                    console.log(result)
                    return alert(result.message);
                }
            })
            .catch(console.warn)
            .finally(() => history.push('/projects'));
    };

    const createProject = () => {

        fetch(`${BASE_URL}/projects`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
            },
            body: JSON.stringify(project)
        })
            .then(result => result.json())
            .then(result => {
                if (result.error) {
                    alert(result.message)
                }
                uploadCover(result);
                alert('Project is created');
                history.push('/projects');

            })
            .catch(err => console.log(err.message))

    }

    return (<>
        <div className="col-lg-5 col-md-6">
            <div className="card login_page shadow rounded border-0">
                <div className="card-body">
                    <h4 className="card-title text-center">Create Project</h4>
                    <form className="login-form mt-4">
                        <div className="row">
                            <div className="col-md-12">
                                <div className="form-group position-relative">
                                    <label>Project Name<span className="text-danger">*</span></label>
                                    <input id="projectname" type="text" className="form-control pl-5" placeholder="Project Name" required="" onChange={e => updateName(e.target.value)} />
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="form-group position-relative">
                                    <label>Description <span className="text-danger">*</span></label>
                                    <textarea id="description" type="textarea" name="text" className="form-control pl-5" placeholder="Description" required="" onChange={e => updateDescription(e.target.value)} />
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="form-group position-relative">
                                    <label>Country <span className="text-danger">*</span></label>
                                    <input id="country" type="text" className="form-control pl-5" placeholder="Country" required="" onChange={e => updateCountry(e.target.value)} />
                                </div>
                            </div>
                            <div className="col-md-12">
                                <input type="file" onChange={e => setFiles(Array.from(e.target.files))} />
                                <div className="col-md-12" onClick={createProject}>
                                    <button className="btn btn-primary btn-block">Create</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </>)
}