
import React, { useEffect, useState } from 'react';
import { CardImg } from 'react-bootstrap';
import { BASE_URL } from '../../Requests/requests';
import propTypes from 'prop-types';
import { useHistory } from 'react-router-dom';
import { Link } from 'react-router-dom';

const ProjectCard = ({ project }) => {

    const history = useHistory();

    const [users, setUsers] = useState([]);

    const url = `${BASE_URL}/users`;

    useEffect(() => {
        fetch(url,
            {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
                }
            })
            .then(result => {
                return result.json();
            })
            .then(result => {
                if (Array.isArray(result)) {
                    setUsers(result);
                }
            })
            .catch(err => console.log(err.message));
    }, [url]);


    return (

        <div className="col-md-6 col-12 mt-2 pt-2">
            <div
                className="card work-container work-modern position-relative overflow-hidden shadow rounded border-0">
                <div className="card-body p-0" onClick={() => history.push(`/projects/${project.id}`)}>
                    <CardImg src={`${BASE_URL}/covers/${project.photoPath}`} className="img-fluid rounded" alt="" />
                    <div className="overlay-work bg-dark"></div>
                    <div className="content">
                        <p className="title text-white center font-weight-bold">{project.name}</p>
                        <small className="text-light">{project.country}</small>
                    </div>
                </div>
            </div>
        </div>
    );
}

ProjectCard.propTypes = {
    project: propTypes.object.isRequired,
};

export default ProjectCard;