import React, { useState } from 'react';
import { BASE_URL } from '../../Requests/requests';
import { Form, Button } from 'react-bootstrap';

const UpdateProject = (props) => {


    const history = props.history;
    const [project, setProject] = useState({
        name: '',
        description: '',
        country: ''
    });

    const updateName = name => setProject({ ...project, name });
    const updateDescription = description => setProject({ ...project, description });
    const updateCountry = country => setProject({ ...project, country });
    const [files, setFiles] = useState([]);

    const uploadCover = (project) => {
        const formData = new FormData();
        if (!files.length) {
            alert('Project is created');
            return;
        }

        formData.append('files', files[0]);
        console.log(files[0])
        fetch(`${BASE_URL}/projects/${project.id}/cover`, {
            method: 'POST',
            headers: {
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
            },
            body: formData,
        })
            .then(r => r.json())
            .then(result => {
                if (result.error) {
                    console.log(result)
                    return alert(result.message);
                }
            })
            .catch(console.warn)
            .finally(() => history.push('/projects'));
    };

    const updateProject = () => {

        fetch(`${BASE_URL}/projects`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
            },
            body: JSON.stringify(project)
        })
            .then(result => result.json())
            .then(result => {
                if (result.error) {
                    alert(result.message)
                }
                uploadCover(result);
                alert('Project is created');
                history.push('/projects');

            })
            .catch(err => console.log(err.message));
    }

    return (
        <col>
            <h4>Edit Project</h4>
            <Form>
                <Form.Group controlId="title">
                    <Form.Label className="col-form-label-sm">Project Ttile</Form.Label>
                    <Form.Control type="text" placeholder="Enter Title" value={project.name} onChange={e => updateProject(e.target.value)} />
                </Form.Group>
                <Form.Group controlId="author">
                    <Form.Label className="col-form-label-sm">Project Description</Form.Label>
                    <Form.Control type="textarea" placeholder="Enter Description" value={project.description} onChange={e => updateDescription(e.target.value)} />
                </Form.Group>
                <Form.Group controlId="cover">
                    <Form.Label className="col-form-label-sm">Upload cover</Form.Label><br />
                    <input type="file" onChange={e => setFiles(Array.from(e.target.files))} />
                </Form.Group>
                <Button className="bg-gradient-theme-left login border-0" color="primary" variant="outline-success" onClick={() => updateProject()}>Edit</Button>

            </Form>
        </col>

    )
}
export default UpdateProject;