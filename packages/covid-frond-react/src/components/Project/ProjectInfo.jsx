import React from 'react';

export default function ProjectInfoComponent() {
    return (<>
        <div className="row align-items-center">
            <div className="col-lg-6 mt-4 pt-2">
                <div className="card work-details rounded bg-light border-0">
                    <div className="card-body">
                        <h5 className="card-title border-bottom pb-3 mb-3">Project Info :</h5>
                        <dl className="row mb-0">
                            <dt className="col-md-4 col-5">Client :</dt>
                            <dd className="col-md-8 col-7 text-muted">Calvin Carlo</dd>

                            <dt className="col-md-4 col-5">Category :</dt>
                            <dd className="col-md-8 col-7 text-muted">Web Design</dd>

                            <dt className="col-md-4 col-5">Date :</dt>
                            <dd className="col-md-8 col-7 text-muted">23rd Sep, 2019</dd>

                            <dt className="col-md-4 col-5">Website :</dt>
                            <dd className="col-md-8 col-7 text-muted">www.yourdomain.com</dd>

                            <dt className="col-md-4 col-5">Location :</dt>
                            <dd className="col-md-8 col-7 text-muted">3/2/64 Mongus Street, UK</dd>
                        </dl>
                    </div>
                </div>
            </div>

            <div className="col-lg-6 mt-4 pt-2">
                <img src="http://shreethemes.in/landrick/layouts/images/work/6.jpg" className="img-fluid rounded" alt="" />
            </div>
        </div>
    </>)
}