import React, { useState, useContext } from 'react';
import { BASE_URL } from '../../Requests/requests';
import * as Icon from 'react-feather';
import UserContext from '../../Providers/userContext';
import { withRouter } from "react-router";



const SignUpComponent = ({ history }) => {

    const [newUser, setNewUser] = useState({});
    const [confirmPassword, setConfirmpassword] = useState("");
    const [files, setFiles] = useState([]);

    const updateEmail = email => setNewUser({ ...newUser, email });
    const updatePassword = password => setNewUser({ ...newUser, password });
    const updateFirstName = firstName => setNewUser({ ...newUser, firstName });
    const updateLastName = lastName => setNewUser({ ...newUser, lastName });
    const updateUsername = userName => setNewUser({ ...newUser, userName });
    const updateCountry = country => setNewUser({ ...newUser, country });

    const uploadAvatar = (id) => {
        const formData = new FormData();
        if (!files.length) {
            return;
        }

        formData.append('files', files[0]);
        fetch(`${BASE_URL}/users/${id}/avatar`, {
            method: 'POST',
            headers: {
            },
            body: formData,
        })
            .then(r => r.json())
            .then(console.log)
            .catch(console.warn)
            .finally(() => history.push('/sign-in'));
    };
    const register = () => {

        if (newUser.password !== confirmPassword) {
            alert(`Your passwords don't match!`);
            return;
        }
        fetch(`${BASE_URL}/users`, {

            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(newUser),
        })
            .then(r => r.json())
            .then(result => {
                if (result.error) {
                    return alert(result.message);
                }
                uploadAvatar(result.id);
            })
            .catch(alert)
            .finally(history.push('/sign-in'));
    };
    const { user } = useContext(UserContext);
    if (user) {
        history.push('/');
    }
    return (<>
        <div className="col-lg-5 col-md-6">
            <div className="card login_page shadow rounded border-0">
                <div className="card-body">
                    <h4 className="card-title text-center">Signup</h4>
                    <form className="login-form mt-4">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group position-relative">
                                    <label>FirstName <span className="text-danger">*</span></label>
                                    <Icon.User data-feather="user" className="fea icon-sm icons" />
                                    <input id="firstname" type="text" className="form-control pl-5" placeholder="First Name" name="s" required="" onChange={e => updateFirstName(e.target.value)} />
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group position-relative">
                                    <label>Last name <span className="text-danger">*</span></label>
                                    <Icon.UserCheck data-feather="user-check" className="fea icon-sm icons" />
                                    <input id="lastname" type="text" className="form-control pl-5" placeholder="Last Name" name="s" required="" onChange={e => updateLastName(e.target.value)} />
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="form-group position-relative">
                                    <label>Username <span className="text-danger">*</span></label>
                                    <Icon.UserCheck data-feather="user-check" className="fea icon-sm icons" />
                                    <input id="username" type="text" className="form-control pl-5" placeholder="Username" name="s" required="" onChange={e => updateUsername(e.target.value)} />
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="form-group position-relative">
                                    <label>Country<span className="text-danger">*</span></label>
                                    <Icon.UserCheck data-feather="user-check" className="fea icon-sm icons" />
                                    <input id="country" type="text" className="form-control pl-5" placeholder="Country" name="s" required="" onChange={e => updateCountry(e.target.value)} />
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="form-group position-relative">
                                    <label>Your Email <span className="text-danger">*</span></label>
                                    <Icon.Mail data-feather="mail" className="fea icon-sm icons" />
                                    <input id="email" type="email" className="form-control pl-5" placeholder="Email" name="email" required="" onChange={e => updateEmail(e.target.value)} />
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group position-relative">
                                    <label>Password <span className="text-danger">*</span></label>
                                    <Icon.Key data-feather="key" className="fea icon-sm icons" />
                                    <input id="password" type="password" className="form-control pl-5" placeholder="Password" required="" onChange={e => updatePassword(e.target.value)} />
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group position-relative">
                                    <label>Confirm Password <span className="text-danger">*</span></label>
                                    <Icon.Key data-feather="key" className="fea icon-sm icons" />
                                    <input id="confirm-password" type="password" className="form-control pl-5" placeholder="Confirm Password" required="" onChange={e => setConfirmpassword(e.target.value)} />
                                </div>
                            </div>
                            <div className="col-md-12">
                                <input id="avatar" type="file" onChange={e => setFiles(Array.from(e.target.files))} />
                            </div>
                            <br />
                            <div className="col-md-12">
                                <button className="btn btn-primary btn-block" onClick={register}>Register</button>
                            </div>
                            <div className="mx-auto">
                                <p className="mb-0 mt-3"><small className="text-dark mr-2">Already have an account ?</small> <a href="auth-login.html" className="text-dark font-weight-bold">Sign in</a></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </>)
}

export default withRouter(SignUpComponent);