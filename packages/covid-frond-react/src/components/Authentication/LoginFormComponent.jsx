import React, { useContext, useState } from "react";
import UserContext from "../../Providers/userContext";
import * as Icon from 'react-feather';
import { BASE_URL } from '../../Requests/requests';
import jwtDecode from 'jwt-decode';
import { withRouter } from "react-router";


const LoginFormComponent = (props) => {

    const { setUser } = useContext(UserContext);
    const history = props.history;
    const [loggedUser, setLoggedUser] = useState({});

    const updateEmail = email => setLoggedUser({ ...loggedUser, email });
    const updatePassword = password => setLoggedUser({ ...loggedUser, password });

    const login = (e) => {

        e.preventDefault();

        fetch(`${BASE_URL}/users/login`, {

            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(loggedUser),
        })
            .then(r => r.json())
            .then(result => {
                if (result.error) {
                    return alert(result.message);
                }

                try {
                    const payload = jwtDecode(result.token);
                    setUser(payload);
                } catch (e) {
                    return alert(e.message);
                }

                localStorage.setItem('token', result.token);
                history.push('/home');
            })
            .catch(alert);
    };


    return (<>
        <div className="col-lg-5 col-md-6">
            <div className="card login-page bg-white shadow rounded border-0">
                <div className="card-body">
                    <h4 className="card-title text-center">Login</h4>
                    <form className="login-form mt-4">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="form-group position-relative">
                                    <label>Your Email <span className="text-danger">*</span></label>
                                    <Icon.User data-feather="user" className="fea icon-sm icons" />
                                    <input id="email" type="email" className="form-control pl-5" placeholder="Email" name="email" required="" onChange={e => updateEmail(e.target.value)} />
                                </div>
                            </div>
                            <div className="col-lg-12">
                                <div className="form-group position-relative">
                                    <label>Password <span className="text-danger">*</span></label>
                                    <Icon.Key data-feather="key" className="fea icon-sm icons" />
                                    <input id="password" type="password" className="form-control pl-5" placeholder="Password" required="" onChange={e => updatePassword(e.target.value)} />
                                </div>
                            </div>

                            <div className="col-lg-12 mb-0" value='LOGIN'>
                                <button className="btn btn-primary btn-block" onClick={login}>Sign in</button>
                            </div>
                            <div className="col-12 text-center">
                                <p className="mb-0 mt-3"><small className="text-dark mr-2">Don't have an account ?</small> <a href="auth-signup.html" className="text-dark font-weight-bold">Sign Up</a></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </>)
}

export default withRouter(LoginFormComponent);