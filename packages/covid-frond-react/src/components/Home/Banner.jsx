import React from 'react';
export default function Banner() {
    return (<>
        <section className="home-slider position-relative">
            <div id="carouselExampleControls" className="carousel slide" data-ride="carousel">
                <div className="carousel-inner">
                    <div className="carousel-item align-items-center active ImageMedia">
                        <div className="bg-overlay"></div>
                        <div className="container">
                            <div className="row justify-content-center">
                                <div className="col-lg-12 text-center">
                                    <div className="title-heading mt-4">
                                        <h6 className="text-light para-dark animated fadeInDownBig animation-delay-1">Private
                        office and Co-working space</h6>
                                        <h1 className="heading mb-3 text-white title-dark animated fadeInUpBig animation-delay-3">
                                            Coworking Space For a Success</h1>
                                        <p className="para-desc text-light para-dark mx-auto animated fadeInUpBig animation-delay-7">
                                            Launch your campaign and benefit from our services</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </>);
}
