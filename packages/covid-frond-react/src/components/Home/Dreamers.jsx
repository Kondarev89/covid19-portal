import React from 'react';
import * as Icon from 'react-feather';

export default function Dreamers() {
    return (<>
        <section className="section">

            <div className="container">
                <div className="card bg-light rounded border-0 overflow-hidden">
                    <div className="row no-gutters align-items-center">
                        <div className="col-lg-6">
                            <img src="http://shreethemes.in/landrick/layouts/images/coworking/cta.jpg" className="img-fluid" alt="" />
                        </div>

                        <div className="col-lg-6 text-center">
                            <div className="card-body section-title p-md-5">
                                <h4 className="title mb-4 mt-4">We Are Creative Dreamers and Co-workers</h4>
                                <p className="text-muted mx-auto para-desc mb-0">Start working with WorkSpace that can provide
                                everything you need to generate awareness, drive traffic, connect.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </>);
}
