import React from 'react';

export default function Comfort() {
    return (<>
        <section className="section bg-light">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-12 text-center">
                        <div className="section-title mb-4 pb-2">
                            <h4 className="title mb-4">Your Comfort is Our Priority</h4>
                            <p className="text-muted para-desc mx-auto mb-0">Start working with <span
                                className="text-primary font-weight-bold">WorkSpace</span> that can provide everything you
                            need to generate awareness, drive traffic, connect.</p>
                        </div>
                    </div>
                </div>

                <div className="row">
                    <div className="col-lg-3 col-md-4 col-6 mt-4 pt-2">
                        <div className="features text-center pt-3 pb-3">
                            <img src="http://shreethemes.in/landrick/layouts/images/icon/wifi.svg" height="50" alt="" />
                            <div className="content mt-3">
                                <h4 className="title-2">Fast Internet</h4>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-3 col-md-4 col-6 mt-4 pt-2">
                        <div className="features text-center pt-3 pb-3">
                            <img src="http://shreethemes.in/landrick/layouts/images/icon/printer.svg" height="50" alt="" />
                            <div className="content mt-3">
                                <h4 className="title-2">Printer & Fax</h4>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-3 col-md-4 col-6 mt-4 pt-2">
                        <div className="features text-center pt-3 pb-3">
                            <img src="http://shreethemes.in/landrick/layouts/images/icon/serving-dish.svg" height="50" alt="" />
                            <div className="content mt-3">
                                <h4 className="title-2">Modern Kitchen</h4>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-3 col-md-4 col-6 mt-4 pt-2">
                        <div className="features text-center pt-3 pb-3">
                            <img src="http://shreethemes.in/landrick/layouts/images/icon/24-hours.svg" height="50" alt="" />
                            <div className="content mt-3">
                                <h4 className="title-2">24 Hr Access</h4>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-3 col-md-4 col-6 mt-4 pt-2">
                        <div className="features text-center pt-3 pb-3">
                            <img src="http://shreethemes.in/landrick/layouts/images/icon/mail2.svg" height="50" alt="" />
                            <div className="content mt-3">
                                <h4 className="title-2">Mail Service</h4>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-3 col-md-4 col-6 mt-4 pt-2">
                        <div className="features text-center pt-3 pb-3">
                            <img src="http://shreethemes.in/landrick/layouts/images/icon/calendar_b.svg" height="50" alt="" />
                            <div className="content mt-3">
                                <h4 className="title-2">Events Space</h4>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-3 col-md-4 col-6 mt-4 pt-2">
                        <div className="features text-center pt-3 pb-3">
                            <img src="http://shreethemes.in/landrick/layouts/images/icon/question.svg" height="50" alt="" />
                            <div className="content mt-3">
                                <h4 className="title-2">Conference Rooms</h4>
                            </div>
                        </div>
                    </div>

                    <div className="col-lg-3 col-md-4 col-6 mt-4 pt-2">
                        <div className="features text-center pt-3 pb-3">
                            <img src="http://shreethemes.in/landrick/layouts/images/icon/coffee-cup.svg" height="50" alt="" />
                            <div className="content mt-3">
                                <h4 className="title-2">Organic Tea & Coffee</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </>);
}
