import React from 'react';

export default function AboutOurComunity() {
    return (<>
        <section className="section">
            <div className="container">
                <div className="row align-items-center">
                    <div className="col-lg-5 col-md-6 col-12">
                        <img src="http://shreethemes.in/landrick/layouts/images/coworking/about.jpg" className="img-fluid rounded" alt="" />
                    </div>

                    <div className="col-lg-7 col-md-6 col-12 mt-4 mt-sm-0 pt-2 pt-sm-0">
                        <div className="section-title ml-lg-4">
                            <h4 className="title mb-4">About Our Community</h4>
                            <p className="text-muted">Start working with <span className="text-primary font-weight-bold">WorkSpace</span> that can provide everything you need to generate awareness, drive traffic,
                            connect. Dummy text is text that is used in the publishing industry or by web designers to
                            occupy the space which will later be filled with 'real' content. This is required when, for
                            example, the final text is not yet available. Dummy texts have been in use by typesetters
                            since the 16th century.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </>);
}
