import React, { useContext } from 'react';
import { NavLink } from 'react-router-dom';
import UserContext from "../../Providers/userContext";
import { LOGOUT_USER } from '../../Requests/requests';
import { withRouter } from "react-router";

const NavigationBar = (props) => {

    const { user,setUser } = useContext(UserContext);
    const history = props.history;

    const logout = () => {
        LOGOUT_USER(setUser)
        history.push('/sign-in')
    }
    const toLogin = () => history.push()
    return (<>
        <header id="topnav" className="defaultscroll sticky">
            <div className="container">
                <div>
                    <NavLink className="logo" to="/home" >
                        <img src="https://upload.wikimedia.org/wikipedia/en/thumb/a/a8/Workspace_Group_logo.svg/1280px-Workspace_Group_logo.svg.png" height="50" alt="" />
                    </NavLink >
                </div>
                <div className="buy-button">
                    {user ? <button className="btn btn-light login-btn-light" onClick={logout}>Sign Out</button>
 :                    <button className="btn btn-light login-btn-light" onClick={() => history.push('/sign-in')}>Sign in</button>
}
        
                </div>

                <div className="menu-extras">
                    <div className="menu-item">
                        <span className="navbar-toggle">
                            <div className="lines">
                                <span></span>
                                <span></span>
                                <span></span>
                            </div>
                        </span>
                    </div>
                </div>

                <div id="navigation">
                    <ul className="navigation-menu">
                        <li><NavLink to="/home">Home</NavLink ></li>
                        <li><NavLink to="/About">About</NavLink ></li>
                        <li><NavLink to="/floor">Floor</NavLink ></li>
                        <li><NavLink to="/profile">Profile</NavLink ></li>
                        <li className="has-submenu">
                            <NavLink to="http://localhost:4000/">Landing</NavLink><span className="menu-arrow"></span>
                            <ul className="submenu megamenu">
                                <li>
                                    <ul>   
                                        {user?   null:
                                    <>  
                                        <li><NavLink to="/sign-in">SignIn</NavLink ></li>
                                        <li><NavLink to="/sign-up">SignUp</NavLink ></li>
                                    </>}           
                                        <li><NavLink to="/projects">Projects</NavLink ></li>
                                        <li><NavLink to="/Service">Service</NavLink ></li>
                                        <li><NavLink to="/cases">Cases</NavLink ></li>
                                        
                            
                                        
                                        <li><NavLink to="/createproject">Create Project</NavLink ></li>
                                        <li><NavLink to="/createfloor">Create Floor</NavLink ></li>
                                        <li><NavLink to="/createcurrentweek">Create Current Week</NavLink ></li>
                                        <li><NavLink to="/createnextweek">Create Next Week</NavLink ></li>

                                    </ul>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </header>
    </>);
}
export default withRouter(NavigationBar);
