import React from 'react';

export default function TrustedClients() {
    return (<>

<section className="py-5 border-bottom border-top">
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-12 text-center">
                        <h5>Trusted client by over 10,000+ of the world’s</h5>
                    </div>
                </div>

                <div className="row mt-5 justify-content-center">
                    <div className="col-lg-2 col-md-2 col-6 text-center">
                        <img src="http://shreethemes.in/landrick/layouts/images/client/amazon.svg" className="avatar avatar-ex-sm" alt="" />
                    </div>

                    <div className="col-lg-2 col-md-2 col-6 text-center">
                        <img src="http://shreethemes.in/landrick/layouts/images/client/google.svg" className="avatar avatar-ex-sm" alt="" />
                    </div>

                    <div className="col-lg-2 col-md-2 col-6 text-center mt-4 mt-sm-0">
                        <img src="http://shreethemes.in/landrick/layouts/images/client/lenovo.svg" className="avatar avatar-ex-sm" alt="" />
                    </div>

                    <div className="col-lg-2 col-md-2 col-6 text-center mt-4 mt-sm-0">
                        <img src="http://shreethemes.in/landrick/layouts/images/client/paypal.svg" className="avatar avatar-ex-sm" alt="" />
                    </div>

                    <div className="col-lg-2 col-md-2 col-6 text-center mt-4 mt-sm-0">
                        <img src="http://shreethemes.in/landrick/layouts/images/client/shopify.svg" className="avatar avatar-ex-sm" alt="" />
                    </div>

                    <div className="col-lg-2 col-md-2 col-6 text-center mt-4 mt-sm-0">
                        <img src="http://shreethemes.in/landrick/layouts/images/client/spotify.svg" className="avatar avatar-ex-sm" alt="" />
                    </div>
                </div>
            </div>
        </section>


    </>);
}
