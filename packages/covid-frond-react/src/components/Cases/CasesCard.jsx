import React from 'react';
import { NavLink } from 'react-router-dom';

export default function CasesCardComponent(props) {
    return (<>
        <div className="col-lg-4 col-md-6 col-12 mt-4 pt-2 business">
            <div className="card blog border-0 work-container work-classic shadow rounded-md overflow-hidden">
                <img src="https://www.seebeetee.com/wp-content/uploads/covid-19-wallpapers-9.jpg" className="img-fluid work-image" alt="" />
                <div className="card-body">
                    <div className="content">
                        <NavLink to="" className="badge badge-primary">Business</NavLink>
                        <h5 className="mt-3"><NavLink to="http://localhost:4000/casesdetail" className="text-dark title">{props.Country}</NavLink></h5>
                <p className="text-muted">Cases:{props.Cases}</p>
                    </div>
                </div>
            </div>
        </div>
    </>)
}
