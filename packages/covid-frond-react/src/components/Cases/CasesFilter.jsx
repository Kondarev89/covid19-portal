import React from 'react';
import { NavLink } from 'react-router-dom';


export default function CasesFIlterComponent() {
    return (<>
        <div className="row">
            <ul className="col container-filter list-unstyled categories-filter text-center mb-0" id="filter">
                <li className="list-inline-item"><NavLink to="#" className="categories border d-block text-dark rounded active"
                    data-filter="*">All</NavLink></li>
                <li className="list-inline-item"><NavLink to="#" className="categories border d-block text-dark rounded"
                    data-filter=".business">Business</NavLink></li>
                <li className="list-inline-item"><NavLink to="#" className="categories border d-block text-dark rounded"
                    data-filter=".marketing">Marketing</NavLink></li>
                <li className="list-inline-item"><NavLink to="#" className="categories border d-block text-dark rounded"
                    data-filter=".finance">Finance</NavLink></li>
                <li className="list-inline-item"><NavLink to="#" className="categories border d-block text-dark rounded"
                    data-filter=".human">Human Research</NavLink></li>
            </ul>
        </div>
    </>)
}
