import React from 'react';
import { CREATE_FLOOR,CREATE_DESK } from "../../Requests/requests";
import * as Icon from 'react-feather';

export default function CreateFloor(props) {
    const appFloor = {
        maxDesks: '',
        distance: '',
        country: '',
        branding: 2,
        designing: 3,
        photography: 4,
        development: 5,
    }
    const appDesk = {
        name: 'hiho',
        meaning: 1,
        avatarPath: 'noAvatar.jpg',
        country: appFloor.country,
        parts: 1,
        branding: 2,
        designing: 3,
        photography: 4,
        development: 5,
    }
    const createFloors = () => {
        CREATE_FLOOR(appFloor.maxDesks, appFloor.distance, appFloor.country);

        for(let i=0; i<appDesk.branding; i++){
            CREATE_DESK(appDesk.name, appDesk.meaning, appDesk.avatarPath, appFloor.country, appFloor.branding)
        }
        for(let i=0; i<appDesk.designing; i++){
            CREATE_DESK(appDesk.name, appDesk.meaning, appDesk.avatarPath, appFloor.country, appFloor.designing)
        }
        for(let i=0; i<appDesk.photography; i++){
            CREATE_DESK(appDesk.name, appDesk.meaning, appDesk.avatarPath, appFloor.country, appFloor.photography)
        }
        for(let i=0; i<appDesk.development; i++){
            CREATE_DESK(appDesk.name, appDesk.meaning, appDesk.avatarPath, appFloor.country, appFloor.development)
        }
    }

    return (<>
        <div className="col-lg-5 col-md-6">
            <div className="card login_page shadow rounded border-0">
                <div className="card-body">
                    <h4 className="card-title text-center">Create Floor</h4>
                    <form className="login-form mt-4">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="form-group position-relative">
                                    <label>Max Desks <span className="text-danger">*</span></label>
                                    <Icon.User data-feather="user" className="fea icon-sm icons" />
                                    <input htmlFor="maxDesks" id="maxDesks" type="number" className="form-control pl-5" placeholder="Max number of desks" name="maxDesks" 
                                    onChange={(e) => { appFloor.maxDesks = e.target.value }} />
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group position-relative">
                                    <label>Distance <span className="text-danger">*</span></label>
                                    <Icon.UserCheck data-feather="user-check" className="fea icon-sm icons" />
                                    <input htmlFor="distance" id="distance" type="number" className="form-control pl-5" placeholder="Distance" name="distance"
                                    onChange={(e) => { appFloor.distance = e.target.value }} />
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group position-relative">
                                    <label>Branding Desks <span className="text-danger">*</span></label>
                                    <Icon.User data-feather="user" className="fea icon-sm icons" />
                                    <input htmlFor="branding" id="branding" type="number" className="form-control pl-5" placeholder="Branding Desks" name="branding" 
                                    onChange={(e) => { appDesk.branding = e.target.value }} />
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group position-relative">
                                    <label>Designing Desk <span className="text-danger">*</span></label>
                                    <Icon.UserCheck data-feather="user-check" className="fea icon-sm icons" />
                                    <input htmlFor="designing" id="designing" type="number" className="form-control pl-5" placeholder="Designing Desks" name="designing"
                                    onChange={(e) => { appDesk.designing = e.target.value }} />
                                </div>
                            </div>  
                            <div className="col-md-6">
                                <div className="form-group position-relative">
                                    <label>Photography Desk <span className="text-danger">*</span></label>
                                    <Icon.UserCheck data-feather="user-check" className="fea icon-sm icons" />
                                    <input htmlFor="photography" id="photography" type="number" className="form-control pl-5" placeholder="Photography Desks" name="photography"
                                    onChange={(e) => { appDesk.photography = e.target.value }} />
                                </div>
                            </div>  
                            <div className="col-md-6">
                                <div className="form-group position-relative">
                                    <label>Development Desk <span className="text-danger">*</span></label>
                                    <Icon.UserCheck data-feather="user-check" className="fea icon-sm icons" />
                                    <input htmlFor="development" id="development" type="number" className="form-control pl-5" placeholder="Development Desks" name="development"
                                    onChange={(e) => { appDesk.development = e.target.value }} />
                                </div>
                            </div>  
                            <div className="col-md-12">
                                <div className="form-group position-relative">
                                    <label>Country <span className="text-danger">*</span></label>
                                    <Icon.Mail data-feather="mail" className="fea icon-sm icons" />
                                    <input htmlFor="country" id="country" type="text" className="form-control pl-5" placeholder="Email" name="country" 
                                    onChange={(e) => { appFloor.country = e.target.value }} />
                                    </div>
                            </div>
                            <div className="col-md-12" onClick={createFloors}>
                                <button className="btn btn-primary btn-block">Create</button>
                            </div>
                            <div className="mx-auto">
                                <p className="mb-0 mt-3"><small className="text-dark mr-2">Already have an account ?</small> <a href="auth-login.html" className="text-dark font-weight-bold">Sign in</a></p>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </>)
}