import React, { useContext, useState } from 'react';
import { UPDATE_DESK} from '../../Requests/requests';
import UserContext from '../../Providers/userContext';
import * as Icon from 'react-feather';
import { Link } from 'react-router-dom';

export default function FloorCardComponent(props) {
    const {user} = useContext(UserContext);
    let userID;
    const appDesk = {
        name: 'hiho',
        meaning: 2,
        avatarPath: 'noAvatar.jpg',
        country: 'Bulgaria',
        parts: 1,
        }
        const updateDesk = (id,meaning) => {
            if(meaning===1) {
                meaning=2;
                userID = user.id;
            }
            else {
                meaning = 1;
                userID = 0;
            }
            UPDATE_DESK(id,appDesk.name, meaning,  appDesk.avatarPath, appDesk.country, appDesk.parts,userID)
            handleUpdate(meaning,userID);

        } 
        const [meaningUpdate, setMeaning] = useState(props.UnderTitle);
        const [userUpdate, setUser] = useState(props.UserName);

        const handleUpdate = (meaningUpdatee, userID) => {
            setMeaning(meaningUpdatee);
            setUser(userID);
        }
    return (<>
        <div className="col-lg-3 col-md-6 col-12 px-0 photography">
            <div
                className="card border-0 work-container work-modern position-relative d-block overflow-hidden rounded-0">
                <div className="card-body p-0">
                    <img src="https://images.unsplash.com/photo-1542546068979-b6affb46ea8f?ixlib=rb-1.2.1&auto=format&fit=crop&w=334&q=80" className="img-fluid" alt="so" />
                    <div className="overlay-work bg-dark"></div>
                    <div className="content">
                        <h5 className="mb-0"><a href="page-work-detail.html" className="text-white title">{props.Title}</a></h5>
                        <h6 className="text-light tag mb-0">{meaningUpdate === 1 ? 'Available' : meaningUpdate  === 2 ? 'Occupied' : 'Forbidden'}</h6>
                    </div>
                    <div className="client">
                        <small className="text-light user d-block"><i className="mdi mdi-account" /> {userUpdate ? userUpdate: "You can sit here"}</small>
                        <small className="text-light date"><i className="mdi mdi-calendar-check" /> {props.Date}</small>
                    </div>
                       <div className="icons text-center" onClick={() => updateDesk(props.id,meaningUpdate)} >
                                <Link href="images/work/13.jpg"
                                    className="text-primary work-icon bg-white d-inline-block rounded-pill mfp-image"><Icon.Code
                                        data-feather="camera" className="fea icon-sm"/></Link>
                            </div>
                    </div>
            </div>
        </div>
    </>)
}
