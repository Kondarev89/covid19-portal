import React, { useContext } from 'react';
import { CREATE_NEXTWEEK } from "../../Requests/requests";
import * as Icon from 'react-feather';
import UserContext from '../../Providers/userContext';

export default function CreateNextWeek(props) {
    const {user} = useContext(UserContext);

    const appWeek = {
        status: 1,
        fromDate: '2020-09-20',
        toDate: '2020-09-26'    
    }
    const createCurrentWeek = () => {
        CREATE_NEXTWEEK(1, appWeek.fromDate, appWeek.toDate,user.id);
    }

    return (<>
        <div className="col-lg-5 col-md-6">
            <div className="card login_page shadow rounded border-0">
                <div className="card-body">
                    <h4 className="card-title text-center">Create Next Week</h4>
                    <form className="login-form mt-4">
                        <div className="row">
                        <div className="col-md-6">
                                <div className="form-group position-relative">
                                    <label>Distance <span className="text-danger">*</span></label>
                                    <Icon.UserCheck data-feather="user-check" className="fea icon-sm icons" />
                                    <input htmlFor="fromDate" id="fromDate" type="date" className="form-control pl-5" placeholder="From Date" name="fromDate"
                                    onChange={(e) => { appWeek.fromDate = e.target.value }} />
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group position-relative">
                                    <label>Max Desks <span className="text-danger">*</span></label>
                                    <Icon.User data-feather="user" className="fea icon-sm icons" />
                                    <input htmlFor="toDate" id="toDate" type="date" className="form-control pl-5" placeholder="To Date" name="toDate" 
                                    onChange={(e) => { appWeek.toDate = e.target.value }} />
                                </div>
                            </div>
                            <div className="col-md-12">
                                <div className="form-group position-relative">
                                    <label>Country <span className="text-danger">*</span></label>
                                    <Icon.Mail data-feather="mail" className="fea icon-sm icons" />
                                    <input htmlFor="status" id="status" type="text" className="form-control pl-5" placeholder="Status" name="status" 
                                    onChange={(e) => { appWeek.status = e.target.value }} />
                                    </div>
                            </div>
                            <div className="col-md-12" onClick={createCurrentWeek}>
                                <button className="btn btn-primary btn-block">Create</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

    </>)
}