import React, { useContext } from 'react';
import UserContext from '../../Providers/userContext';

export default function ProfileBanner({
    rounded,
    circle,
    src,
    size,
    tag: Tag,
    className,
    style,
}) {

    const { user } = useContext(UserContext);


    return (<>
        <section className="bg-profile d-table w-100  ProfileBegginingImage" >
            <div className="container">
                <div className="row">
                    <div className="col-lg-12">
                        <div className="card public-profile border-0 rounded shadow ProfileBeggining">
                            <div className="card-body">
                                <div className="row align-items-center">
                                    <div className="col-lg-2 col-md-3 text-md-left text-center">
                                        <Tag className="avatar avatar-large rounded-circle shadow d-block mx-auto" alt="" src={user.avatarPath}
                                            style={{ width: size, height: size, ...style }} />
                                    </div>

                                    <div className="col-lg-10 col-md-9">
                                        <div className="row align-items-end">
                                            <div className="col-md-7 text-md-left text-center mt-4 mt-sm-0">
                                                <h3 className="title mb-0">{user.firstName} {user.lastName}</h3>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    </>)
}

ProfileBanner.defaultProps = {
    tag: 'img',
    rounded: false,
    circle: true,
    size: 40,
    src: '',
    style: {},
};