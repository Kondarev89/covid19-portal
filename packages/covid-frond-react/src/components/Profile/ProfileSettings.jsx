import React, { useState, useContext } from 'react';
import { Button } from 'react-bootstrap';
import { NavLink } from 'react-router-dom';
import { BASE_URL } from '../../Requests/requests';
import UserContext from '../../Providers/userContext';
;
export default function ProfileSettingsComponent() {

    const { user } = useContext(UserContext);
    const [files, setFiles] = useState([]);

    const formData = new FormData();
    if (!files.length) {
        return;
    }

    formData.append('files', files[0]);
    fetch(`${BASE_URL}/users/${user.id}/avatar`, {
        method: 'POST',
        headers: {
        },
        body: formData,
    })
        .then(r => r.json())
        .then(console.log)
        .catch(console.warn);

    return (<>


        <div className="col-lg-8 col-12">
            <div className="card border-0 rounded shadow">
                <div className="card-body">
                    <h5 className="text-md-left text-center">Personal Detail :</h5>

                    <div className="mt-3 text-md-left text-center d-sm-flex">
                        <img src="http://shreethemes.in/landrick/layouts/images/client/05.jpg"
                            className="avatar float-md-left avatar-medium rounded-circle shadow mr-md-4" alt="" />

                        <div className="mt-md-4 mt-3 mt-sm-0">
                            <Button to="#" className="btn btn-primary mt-2" onChange={e => setFiles(Array.from(e.target.files))}>Change Picture</Button>
                            <NavLink to="#" className="btn btn-outline-primary mt-2 ml-2">Delete</NavLink>
                        </div>
                    </div>

                    <form>
                        <div className="row mt-4">
                            <div className="col-md-6">
                                <div className="form-group position-relative">
                                    <label>First Name</label>
                                    <i data-feather="user" className="fea icon-sm icons" />
                                    <input name="name" id="first" type="text" className="form-control pl-5"
                                        placeholder="First Name :" />
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group position-relative">
                                    <label>Last Name</label>
                                    <i data-feather="user-check" className="fea icon-sm icons" />
                                    <input name="name" id="last" type="text" className="form-control pl-5"
                                        placeholder="Last Name :" />
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group position-relative">
                                    <label>Your Email</label>
                                    <i data-feather="mail" className="fea icon-sm icons" />
                                    <input name="email" id="email" type="email" className="form-control pl-5"
                                        placeholder="Your email :" />
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="form-group position-relative">
                                    <label>Occupation</label>
                                    <i data-feather="bookmark" className="fea icon-sm icons" />
                                    <input name="name" id="occupation" type="text" className="form-control pl-5"
                                        placeholder="Occupation :" />
                                </div>
                            </div>
                            <div className="col-lg-12">
                                <div className="form-group position-relative">
                                    <label>Description</label>
                                    <i data-feather="message-circle" className="fea icon-sm icons" />
                                    <textarea name="comments" id="comments" rows="4" className="form-control pl-5"
                                        placeholder="Description :"></textarea>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12">
                                <input type="submit" id="submit" name="send" className="btn btn-primary"
                                    value="Save Changes" />
                            </div>
                        </div>
                    </form>


                    <div className="row">
                        <div className="col-md-6 mt-4 pt-2">
                            <h5>Contact Info :</h5>

                            <form>
                                <div className="row mt-4">
                                    <div className="col-lg-12">
                                        <div className="form-group position-relative">
                                            <label>Phone No. :</label>
                                            <i data-feather="phone" className="fea icon-sm icons" />
                                            <input name="number" id="number" type="number"
                                                className="form-control pl-5" placeholder="Phone :" />
                                        </div>
                                    </div>

                                    <div className="col-lg-12">
                                        <div className="form-group position-relative">
                                            <label>Website :</label>
                                            <i data-feather="globe" className="fea icon-sm icons" />
                                            <input name="url" id="url" type="url" className="form-control pl-5"
                                                placeholder="Url :" />
                                        </div>
                                    </div>

                                    <div className="col-lg-12 mt-2 mb-0">
                                        <button className="btn btn-primary">Add</button>
                                    </div>
                                </div>
                            </form>
                        </div>

                        <div className="col-md-6 mt-4 pt-2">
                            <h5>Change password :</h5>
                            <form>
                                <div className="row mt-4">
                                    <div className="col-lg-12">
                                        <div className="form-group position-relative">
                                            <label>Old password :</label>
                                            <i data-feather="key" className="fea icon-sm icons" />
                                            <input type="password" className="form-control pl-5"
                                                placeholder="Old password" required="" />
                                        </div>
                                    </div>

                                    <div className="col-lg-12">
                                        <div className="form-group position-relative">
                                            <label>New password :</label>
                                            <i data-feather="key" className="fea icon-sm icons" />
                                            <input type="password" className="form-control pl-5"
                                                placeholder="New password" required="" />
                                        </div>
                                    </div>

                                    <div className="col-lg-12">
                                        <div className="form-group position-relative">
                                            <label>Re-type New password :</label>
                                            <i data-feather="key" className="fea icon-sm icons" />
                                            <input type="password" className="form-control pl-5"
                                                placeholder="Re-type New password" required="" />
                                        </div>
                                    </div>

                                    <div className="col-lg-12 mt-2 mb-0">
                                        <button className="btn btn-primary">Save password</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </>)
}