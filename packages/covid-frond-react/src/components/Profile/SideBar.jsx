import React, { useState } from 'react';
import ProfileInfoComponent from './ProfileInfoComponent';
import ProfileSettingsComponent from './ProfileSettings';

export default function ProfileSideBar(props) {

    return (<>
        <div className="sidebar sticky-bar p-4 rounded shadow">
            <ProfileInfoComponent />
        </div>
    </>)
}