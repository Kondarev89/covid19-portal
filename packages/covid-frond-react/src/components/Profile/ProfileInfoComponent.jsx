
import React, { useContext, useEffect, useState } from 'react';
import { NavLink } from 'react-router-dom';
import UserContext from '../../Providers/userContext';
import { BASE_URL } from '../../Requests/requests';

const ProfileInfoComponent = ({ match }) => {
    const usery = useContext(UserContext);

    const [user, setUser] = useState([]);

    useEffect(() => {
        fetch(`${BASE_URL}/users/${user.usery}`,
            {
                headers: {
                    Authorization: `Bearer ${localStorage.getItem('token') || ''}`,
                }
            })
            .then(result => {
                return result.json();
            })
            .then(result => {
                if (result) {
                    setUser(result);
                }
            }).catch(err => console.log(err.message));
    }, []);


    return (<>

        <div className="col-lg-8 col-12">

            <h5>{user.firstName} {user.lastName}</h5>
            <div className="row">

            </div>
        </div>
        <div className="col-md-6 col-12 mt-2 pt-2">
            <div
                className="card work-container work-modern position-relative overflow-hidden shadow rounded border-0">
                <div className="card-body p-0">
                    <img src="http://shreethemes.in/landrick/layouts/images/work/1.jpg" className="img-fluid rounded" alt="aham" />
                    <div className="overlay-work bg-dark"></div>
                    <div className="content">
                        <NavLink to="page-work-detail.html"
                            className="title text-white d-block font-weight-bold">{user.projects}</NavLink>
                    </div>
                </div>
            </div>
        </div>
        <div className="col-md-6 mt-4">
            <h5>Personal Details :</h5>
            <div className="mt-4">
                <div className="media align-items-center">
                    <i data-feather="mail" className="fea icon-ex-md text-muted mr-3" />
                    <div className="media-body">
                        <h6 className="text-primary mb-0">Email :</h6>
                        <span className="text-muted">{user.email}</span>
                    </div>
                </div>

                <div className="media align-items-center mt-3">
                    <i data-feather="gift" className="fea icon-ex-md text-muted mr-3" />
                    <div className="media-body">
                        <h6 className="text-primary mb-0">Username :</h6>
                        <p className="text-muted mb-0">{user.username}</p>
                    </div>
                </div>
                <div className="media align-items-center mt-3">
                    <i data-feather="map-pin" className="fea icon-ex-md text-muted mr-3" />
                    <div className="media-body">
                        <h6 className="text-primary mb-0">Location :</h6>
                        <span className="text-muted">{user.country}</span>
                    </div>
                </div>
                <div className="media align-items-center mt-3">
                    <i data-feather="map-pin" className="fea icon-ex-md text-muted mr-3" />
                    <div className="media-body">
                        <h6 className="text-primary mb-0">Status :</h6>
                        {/* <span className="text-muted">{user.country}</span> */}
                    </div>
                </div>


            </div>
        </div>
    </>)
}

export default ProfileInfoComponent;