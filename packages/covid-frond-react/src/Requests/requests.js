import { getLoggedUser, getToken } from "../Providers/userContext";

export const BASE_URL = 'http://localhost:3000';

const AuthHeader = () => `Bearer ${getToken()}`;

export const handleErr = (err) => {
    if (err?.error) {
        throw new Error(err.message)
    }

    return err;
}

export const LOGOUT_USER = (setUser) => {
    fetch(`${BASE_URL}/users/logout`, {
        method: 'DELETE',
        headers: {
            'Authorization': AuthHeader(),
            'Content-Type': 'application/json'
        }
    })
        .then(res => res.json())
        .then(res => handleErr(res))
        .then(res => localStorage.removeItem('token'))
        .then(res => setUser(null))
        .catch(console.error)
}

export const CREATE_PROJECT = (name, description, formData, files) => {

    fetch(`${BASE_URL}/books`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
        body: JSON.stringify({
            name: name,
            description: description
        }),

    })
        .then(res => res.json())
        .then(res => handleErr(res))
        .catch(err => alert(err))
};

export const GET_ALL_PROJECTS = (setBooks) => {
    fetch(`${BASE_URL}/books`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
    })
        .then(response => response.json())
        .then(res => handleErr(res))
        .then(result => setBooks(result))
        .catch(console.error)
};


export const SET_CARDS = (setCards, sortFn, query) => {
    fetch(`${BASE_URL}/books`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
    })
        .then(response => response.json())
        .then(res => handleErr(res))
        .then(res => sortFn(res, query))
        .then(result => setCards(result))
        .catch(console.error)
};


export const GET_ALL_USERS = (setUsers) => {
    fetch(`${BASE_URL}/users`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
    })
        .then(response => response.json())
        .then(res => handleErr(res))
        .then(result => setUsers(result))
        .catch(console.error)
};

export const CREATE_FLOOR = (maxDesks, distance, country) => {

    fetch(`${BASE_URL}/floors`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
        body: JSON.stringify({
            maxDesks: maxDesks,
            distance: distance,
            country: country
        }),

    })
        .then(res => res.json())
        .then(res => handleErr(res))
        .catch(err => alert(err))
};
export const GET_ALL_FLOORS = (setFloors) => {
    fetch(`${BASE_URL}/floors`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
    })
        .then(response => response.json())
        .then(res => handleErr(res))
        .then(result => setFloors(result))
        .catch(console.error)
};

export const CREATE_DESK = (name, meaning, avatarPath,country,parts) => {
    fetch(`${BASE_URL}/desks`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
        body: JSON.stringify({
            name: name,
            meaning: meaning,
            avatarPath: avatarPath,
            country: country,
            parts: parts,
            userId: 0
        }),

    })
        .then(res => res.json())
        .then(res => handleErr(res))
        .catch(err => alert(err))
};

export const GET_ALL_DESKS = (setDesks) => {
    fetch(`${BASE_URL}/desks`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
    })
        .then(response => response.json())
        .then(res => handleErr(res))
        .then(result => setDesks(result))
        .catch(console.error)
};
export const GET_ALL_COVID = (setCovid) => {
    fetch(`${BASE_URL}/covid`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
    })
        .then(response => response.json())
        .then(res => handleErr(res))
        .then(result => setCovid(result))
        .catch(console.error)
};
export const UPDATE_DESK = (id, name, meaning, avatarPath,country,parts,userId) => {

        fetch(`${BASE_URL}/desks/${id}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${getToken()}`,
            },
            body: JSON.stringify({              
                  userId: userId,
                name: name,
                meaning: meaning,
                avatarPath: avatarPath,
                country: country,
                parts: parts,
            }),
         
        })
            .then(res => res.json())
            .then(res => handleErr(res))
            .catch(err => alert(err))
    };
    export const UPDATE_DESK_MEANING = (id, meaning) => {
        fetch(`${BASE_URL}/desks/meaning/${id}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${getToken()}`,
            },
            body: JSON.stringify({              
                meaning: meaning,
            }),
        })
            .then(res => res.json())
            .then(res => handleErr(res))
            .catch(err => alert(err))
    };

    export const CREATE_CURRENTWEEK = (status, fromDate, toDate,userid) => {
        fetch(`${BASE_URL}/currentweek`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${getToken()}`,
            },
            body: JSON.stringify({
                status: status,
                fromDate: fromDate,
                toDate: toDate,
                userid: userid
            }),
    
        })
            .then(res => res.json())
            .then(res => handleErr(res))
            .catch(err => alert(err))
    };

    export const CREATE_NEXTWEEK = (status, fromDate, toDate,userid) => {
        fetch(`${BASE_URL}/nextWeek`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${getToken()}`,
            },
            body: JSON.stringify({
                status: status,
                fromDate: fromDate,
                toDate: toDate,
                userid: userid
            }),
    
        })
            .then(res => res.json())
            .then(res => handleErr(res))
            .catch(err => alert(err))
    };