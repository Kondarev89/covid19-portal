import React from 'react';
import { Redirect, Route } from 'react-router-dom';

const ProtectedRoute = ({ component: Component, user, ...rest }) => (
    <Route
      {...rest} render={(props) => (
          user
              ? <Component {...props} />
              : <Redirect to='/home' />
      )}
    />
  );

export default ProtectedRoute;