import { createContext } from 'react';
import jwtDecode from 'jwt-decode';



export const getLoggedUser = () => {
  try {
    return jwtDecode(localStorage.getItem('token') || '');
  } catch (e) {
    localStorage.removeItem('token');
    return null;
  }
}

export const getToken = () => {
  return localStorage.getItem('token');
};

const UserContext = createContext({
  user: null,
  isLoggedIn: false,
  setUser: () => { },
});

export default UserContext;
